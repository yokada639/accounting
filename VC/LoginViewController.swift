//
//  LoginViewController.swift
//  記帳
//
//  Created by Murphy on 2024/5/9.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var accountTextfieldView: UIView!
    
    @IBOutlet weak var accountTextfield: UITextField!
    
    @IBOutlet weak var passwordTextfieldView: UIView!
    
    @IBOutlet weak var passwordTextfield: UITextField!
        
    var backToMyLedgerClosure: ()->() = {}
    
//    var goToLedgerListClosure: ()->() = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDelegate()
        self.setFrames()
    }
    
    /**
     點空白處收鍵盤（碰到螢幕時觸發）
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    
    //MARK: - functions
    /**
     畫面delegate設定
     */
    func setDelegate(){
        accountTextfield.delegate = self
        passwordTextfield.delegate = self
    }
    
    /**
     設定frame
     */
    func setFrames(){
        accountTextfieldView.layer.cornerRadius = accountTextfieldView.frame.height/2
        passwordTextfieldView.layer.cornerRadius = passwordTextfieldView.frame.height/2
        self.backgroundView.layer.cornerRadius = 25
    }


    //MARK: - IBActions
    /**
     btn - 忘記密碼
     */
    @IBAction func forgetPasswordBtn(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: "請輸入E-mail", preferredStyle: .alert)
        alertController.addTextField()
        let sendAction = UIAlertAction(title: "送出", style: .default){ [weak self] _ in
            //用index取alertController上第一個textField: alertController.textFields?[0]
            guard let userEmail = alertController.textFields?[0].text else { return }
            Auth.auth().sendPasswordReset(withEmail: userEmail) { [weak self] error in
                let resetAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default)
                resetAlertController.addAction(okAction)
                if error == nil{
                    resetAlertController.message = "發送成功"
                }
                else{
                    resetAlertController.message = "發送失敗"
                }
                self?.present(resetAlertController, animated: true)
            }
        }
        alertController.addAction(sendAction)
        self.present(alertController, animated: true)
    }
    
    /**
     btn - 密碼可見按鈕
     */
    @IBAction func isVisibleBtn(_ sender: UIButton) {
        if passwordTextfield.isSecureTextEntry{
            sender.setImage(UIImage(named: "visible-48"), for: .normal)
            passwordTextfield.isSecureTextEntry = false
        }
        else{
            sender.setImage(UIImage(named: "invisible-48"), for: .normal)
            passwordTextfield.isSecureTextEntry = true
        }
    }
    
    /**
     btn - 註冊
     */
    @IBAction func registerAccountBtn(_ sender: UIButton) {
        if let registerVC = UIStoryboard(name: "RegisterViewController", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController{
            registerVC.dismissLoginVCClosure = { [weak self] in
                self?.dismiss(animated: false)
//                self?.goToLedgerListClosure()
            }
            self.present(registerVC, animated: true)
        }
//        navigationController?.pushViewController(registerVC, animated: true)
    }
    
    /**
     btn - 登入
     */
    @IBAction func loginBtn(_ sender: UIButton) {
        if let email = accountTextfield.text, let password = passwordTextfield.text{
            Auth.auth().signIn(withEmail: email, password: password) { [weak self] result, error in
                guard error == nil else {
                    let alertController = UIAlertController(title: "Oops! 登入失敗", message: "請重新輸入帳號密碼", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] action in
                        guard let selfVC = self else{ return }
                        selfVC.accountTextfield.text = ""
                        selfVC.passwordTextfield.text = ""
                    }
                    alertController.addAction(okAction)
                    self?.present(alertController, animated: true)
                    return
                }
                
                //登入成功收起畫面，並回到頁面
                self?.dismiss(animated: true)
//                self?.goToLedgerListClosure()
            }
        }
        
    }
    
    /**
     返回我的帳本
     */
    @IBAction func backToMyLedger(_ sender: UIButton) {
//        self.tabBarController?.tabBar.isHidden = false
        self.backToMyLedgerClosure()
        self.dismiss(animated: true)
        

//        self.navigationController?.popToRootViewController(animated: false)
       
//        self.definesPresentationContext = true
//        myLedgerVC.modalPresentationStyle = .overCurrentContext
//        myLedgerVC.tabBarController?.tabBar.isHidden = false
//        present(myLedgerVC, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - extension UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate{
    /**
     用return鍵收鍵盤
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // 收起鍵盤
        return true
    }
}
