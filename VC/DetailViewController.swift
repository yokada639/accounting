//
//  DetailViewController.swift
//  記帳
//
//  Created by Murphy on 2024/4/5.
//

import UIKit
import CoreData
import FirebaseAuth
import Firebase
import FirebaseCore
import GoogleSignIn

//tabbar內每一個ViewController的分頁第一頁，全部都是繼承UIViewController
class DetailViewController: UIViewController {

    @IBOutlet weak var editViewHeight: NSLayoutConstraint!
    @IBOutlet weak var editView: InputView!
    var inputHeight: CGFloat = 0.0
    
 
    @IBOutlet weak var dataTableView: UITableView!
    
    @IBOutlet weak var mask: UIView!
    
    weak var delegate: DetailViewControllerDelegate?
    
//  MARK: 用來操作core data的常數
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var originDataList:[DataInfo]?
    var cellUIDataList:[CellInfo]?
    
    var showEditViewHeight:CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataTableView.layer.cornerRadius = 25
        //把navigation back按鈕隱藏
//        self.navigationItem.hidesBackButton = true
            //firebase 匿名登入
//            Auth.auth().signInAnonymously { [weak self] (user, error) in
//                if error == nil{
//                    print("test user ID: \(user!.user.uid)")
//                }
//                else{
//                    print("test Error msg: \(error!.localizedDescription)")
//                }
//            }


        //設定上方navigation正中間的文字
        self.navigationItem.title = "我的帳本"
        getOriginCoreDataList()
        setOriginDataToUIData()
        
 
        setDelegateSettings()
        
        //設定輸入匡的高度
        inputHeight = editView.btnOutsideViewHeight.constant + 80
        mask.backgroundColor = UIColor(red: 30/255, green: 30/255, blue: 30/255, alpha: 0.2)
        editView.setNeedsLayout()
        editView.layoutIfNeeded()
  
//      MARK: 設定editView closures
        editView.presentDatePickerClosure = { [weak self] dateString in
            guard let selfVC = self else { return }
            selfVC.presentDatePickerClosureImpl(date: dateString)
        }
        editView.addDataClosure = { [weak self] newData in
            guard let selfVC = self else { return }
            selfVC.addDataClosureImpl(newData)
        }
        editView.updateDataClosure = { [weak self] (newData, objectId) in
            guard let selfVC = self else { return }
            if let objId = objectId{
                selfVC.updateDataClosureImpl(newData, objectId: objId)
            }
        }
        editView.closeEditViewClosure = { [weak self] in
            guard let selfVC = self, selfVC.editViewHeight.constant != 0 else { return }
            selfVC.closeEditViewClosureImpl()
        }
        
        editView.pushEditTagsVCClosure = { [weak self] in
            guard let selfVC = self else { return }
            selfVC.pushEditTagsVCClosureImpl()
            
        }

        
        //delegate 關閉editView
        editView.delegate = self
        
    }
//MARK: - 筆記start
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.editView.getItemTags()
//        self.editView.itemTableView.reloadData()
//    }
    /**
        用同一個UI做到看起來像不同分頁（present滿版）
     */
//    @IBAction func changeMode(_ sender: UIButton) {
//        let sb = UIStoryboard.init( name: "Main" , bundle: nil )
//        let vc: DetailViewController = sb.instantiateViewController( withIdentifier: "DetailViewController" ) as! DetailViewController
//        vc.isShare = true
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: false)
//    }
    
    /**
        用同一個UI Controller從view did load送data設定
     */
//    func setUIColor(backColor:UIColor,maskColor:UIColor){
//        self.view.backgroundColor = backColor
//        self.mask.backgroundColor = maskColor
//    }
//MARK: 筆記end
//MARK: - IBActions
    /**
        彈出輸入資料的VC
     */
    @IBAction func addNewData(_ sender: Any) {
        showEditView(objectID: nil)
    }
    
    /**
     彈出輸入資料的VC（objectID == nil：新增 / objectID != nil：修改）
     */
    func showEditView(objectID: NSManagedObjectID?){
        editView.reload()
        if editViewHeight.constant == 0{
            editView.isHidden = false
            UIView.animate(withDuration: 0.1) {
                self.editViewHeight.constant = self.inputHeight
                self.editView.setNeedsLayout()
                self.editView.layoutIfNeeded()
                self.mask.isHidden = false
            }
        }
        
        if(objectID != nil){
            if let instanse = getAssignOriginCoreData(objectId: objectID!) as? DataInfoDetail{
                editView.price.text = String(instanse.price)
                editView.itemImg.image = UIImage(named: (instanse.itemImg ?? ""))
                editView.itemLabel.text = instanse.item
                editView.dateBtn.setTitle(instanse.dataInfo?.date, for: .normal)
                editView.remarkTextfield.text = instanse.remark
                editView.cellObjectId = objectID
            }

        }
/**        editView.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.editViewHeight.constant = self.showEditViewHeight ?? 0
            //讓畫面再次重新設置新設定的layout
            self.editView.setNeedsLayout()
            self.editView.layoutIfNeeded()
        }
        
        //畫面reload
        editView.reload()

        //用viewController的做法
        let editorSB = UIStoryboard(name: "EditorViewController", bundle: nil)
        let editorVC = editorSB.instantiateViewController(identifier: "EditorViewController") as! EditorViewController
        present(editorVC, animated: true, completion: nil)
 */
    }

    /**
     讀取coredata資料：顯示在TableView上
     */
    func getOriginCoreDataList(){
        //取得core data資料
        do {
            let fetchRequest : NSFetchRequest<DataInfo> = DataInfo.fetchRequest()
            //對NSSet做排序：[NSSortDescriptor(key: (String?變數名), ascending: (Bool true升序 false降序))]
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
            self.originDataList = try context.fetch(fetchRequest)
            print("取得core data資料成功\r\n")
        }catch{
            print("取得core data資料失敗\r\n")
            fatalError("Failed to fetch data: \(error)")
        }
    }
    
//   MARK: - 取得指定core data原始資料
//      data:篩選來源
//      action：欲執行之動作
    /**
    用objectID 取core data資料
     */
    func getAssignOriginCoreData(objectId: NSManagedObjectID? = nil) -> NSManagedObject?{
        do {
            if let objId = objectId{
                let coreDataObject = try context.existingObject(with: objId)
                return coreDataObject
            }
            else{
                return nil
            }
            
        } catch {
            print("無法找到相應的 NSManagedObject：\(error)")
        }
        
        return nil
        
    }
    /**
     把Core Data Entity資料轉成UI用資料
     */
    func setOriginDataToUIData(){
        self.cellUIDataList = [CellInfo]()
        //抓section資料
        if let originDataList = self.originDataList{
            for originData in originDataList{
                //UI用資料
                let cellInfo = CellInfo()
                let date = originData.date
                let totalPrice = String(originData.totalPrice)
                let superObjectId = originData.objectID
                var cellInfoDetailList : [CellInfoDetail] = [CellInfoDetail]()
                var cellInfoDetail: CellInfoDetail?
                
                //抓detail資料
                //data.dataInfoDetail是NSSet,轉型為原始core data[DataInfoDetail]資料
                if let originDetailList = (originData.dataInfoDetail?.allObjects) as? [DataInfoDetail]{
                    for originDetail in originDetailList{
                        cellInfoDetail = CellInfoDetail()
                        if let item = originDetail.item{
                            cellInfoDetail?.item = item
                        }
                        cellInfoDetail?.price = String(originDetail.price)
                        if let remark = originDetail.remark{
                            cellInfoDetail?.remark = remark
                        }
                        
                        if let newData = cellInfoDetail{
                            cellInfoDetailList.append(newData)
                        }
                        if let itemImg = originDetail.itemImg{
                            cellInfoDetail?.itemImg = itemImg
                        }
                        cellInfoDetail?.superClassObjectId = superObjectId
                        //core data資料的id
                        cellInfoDetail?.objectId = originDetail.objectID
                    }
                }
                
                //cellInfo物件賦值
                cellInfo.date = date
                cellInfo.totalPrice = totalPrice
                cellInfo.detailData = cellInfoDetailList
                cellInfo.objectId = superObjectId
                
                self.cellUIDataList?.append(cellInfo)
            }
        }
    }
}

//  MARK: - extension UITableViewDelegate, UITableViewDataSource
extension DetailViewController: UITableViewDelegate, UITableViewDataSource{
    /**
     tableView內有幾個section
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return cellUIDataList?.count ?? 0
    }
    
    /**
     一個section有幾個cell(資料數量)
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dataInfo = cellUIDataList?[section], let dataInfoDetail = dataInfo.detailData{
            return dataInfoDetail.count
        }
        return 0
        
    }
    
    /**
     設定cell的內容
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //設定tableView的cell用指定xib來製作, 須轉型為該xib class  **identifier = cell檔名＆class名
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell", for: indexPath) as! DetailTableViewCell
        //利用indexPath將資料一筆一筆傳入Cell
        if let detail = cellUIDataList?[indexPath.section].detailData?[indexPath.row] {
            cell.setCellDetail(cellInfoDetail: detail)
        }
        return cell
    }
    //修改section header文字
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return cellUIDataList?[section].date
    }
    

    /**
     用UIView當Section
     */
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let labelColor = UIColor(red: 25/255.0, green: 25/255.0, blue: 25/255.0, alpha: 1)
        //section的view
        let sectionView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        sectionView.backgroundColor = .white
        
        //設定兩個label
        let dateLabel = UILabel(frame: CGRect(x: 30, y: 10 , width: tableView.frame.width/2, height: 35))
        dateLabel.text = cellUIDataList?[section].date
        dateLabel.font = UIFont.boldSystemFont(ofSize: 25)
        dateLabel.textColor = labelColor
        sectionView.addSubview(dateLabel)
        
        let priceLabel = UILabel(frame: CGRect(x: tableView.frame.width/2, y: 10 , width: tableView.frame.width/2 - 30, height: 35))
        priceLabel.text = cellUIDataList?[section].totalPrice
        priceLabel.font = UIFont.boldSystemFont(ofSize: 25)
        priceLabel.textColor = labelColor
        priceLabel.textAlignment = .right
        sectionView.addSubview(priceLabel)
        
        //section下面的分隔線
        let underLine = UIView(frame: CGRect(x: 15, y: sectionView.frame.height - 1, width: sectionView.frame.width - 30, height: 2))
        underLine.backgroundColor = labelColor
        sectionView.addSubview(underLine)

        return sectionView
    }
    
    /**
     設定section header的高度（如果用UIView當section，調成跟UIView的高度一樣）
     */
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    /**
    cell左滑刪除
     */
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //設定刪除Action（cell左滑）
        let deleteAction = UIContextualAction(style: .destructive, title: "刪除") { [weak self] (action, view, completion) in
            //先刪除core data資料 用core data的objectID抓
            //objectId：coredata ID / detailObj：coredata實體 / detail：實體class
            //superObjectId：coredata ID(父) / dataObj：coredata實體(父) / data：實體class(父)
            if let objectId = self?.cellUIDataList?[indexPath.section].detailData?[indexPath.row].objectId, //objectId：coredata ID
               let detailObj = self?.getAssignOriginCoreData(objectId: objectId), //detailObj：coredata實體
                let detail = detailObj as? DataInfoDetail{ //detail：實體class
                if let superObjectId = self?.cellUIDataList?[indexPath.section].detailData?[indexPath.row].superClassObjectId, //superObjectId：coredata ID(父)
                   let dataObj = self?.getAssignOriginCoreData(objectId: superObjectId), //dataObj：coredata實體(父)
                   let data = dataObj as? DataInfo{ //data：實體class(父)
                    do{
                        //先做總金額扣除並儲存(update)
                        data.totalPrice = data.totalPrice - detail.price
                        try self?.context.save()
                    }catch{
                        print("儲存失敗")
                    }
                    //刪除core data的detail實體(delete)並儲存（不然會造成core data資料未及時更新導致奇怪問題）
                    self?.context.delete(detailObj)
                    self?.appDelegate.saveContext()
                    
                    //抓當下core data父實體是否還有detail,沒有的話連父實體都要刪除
                    if let currentData = dataObj as? DataInfo {
                        if(currentData.dataInfoDetail?.count == 0){
                            self?.context.delete(dataObj)
                        }
                    }
                    self?.appDelegate.saveContext()
                }

            }
            //刪除UI資料來源: [CellInfo]?   (先把core data處理好再處理UI避免出錯 或根本不用處理UI重新reload就好）
//            self.cellUIDataList?[indexPath.section].detailData?.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: .automatic)
            completion(true)
            
            //重新取得core data資料並轉為UI可用的class再reload畫面
            self?.getOriginCoreDataList()
            self?.setOriginDataToUIData()
            self?.dataTableView.reloadData()
        }
        deleteAction.backgroundColor = .red
        
        //修改
        let edit = UIContextualAction(style: .normal, title: "修改") { [weak self] (action, view, completion) in
            if let objectId = self?.cellUIDataList?[indexPath.section].detailData?[indexPath.row].objectId{
                self?.showEditView(objectID: objectId)
            }
               
            
            completion(true)
        }
        edit.backgroundColor = UIColor(red: 67.0/255, green: 67.0/255, blue: 113.0/255, alpha: 1)
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, edit])

        return configuration
    }
    
    /**
     設定table view用的nib / delegate / dataSource
     */
    func setDelegateSettings(){
        let tableViewNib = UINib(nibName: "DetailTableViewCell", bundle: nil)
        self.dataTableView.register(tableViewNib, forCellReuseIdentifier: "DetailTableViewCell")
        dataTableView.delegate = self
        dataTableView.dataSource = self
    }
    
    /**
    cell被點擊後的行為
     */
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.showEditView(detail: cellUIDataList?[indexPath.section].detailData?[indexPath.row])
//
//    }
    
    
    /**
    Cell左滑刪除(可用 但會出現'UITableViewRowAction' was deprecated in iOS 13.0: Use UIContextualAction and related APIs instead.提醒)
     */
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let deleteAction = UITableViewRowAction(style: .default, title: "刪除") {
//            // 按下 Delete 之後要執行的動作
//            (deleteAction: UITableViewRowAction, indexPath: IndexPath) in
//            self.cellUIDataList?[indexPath.section].detailData?.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
//
//        }
//        deleteAction.backgroundColor = .red
//        return [deleteAction]
//
//    }
}

//  MARK: - extension InputViewDelegate
extension DetailViewController: InputViewDelegate{
    /**
     editView下縮
     */
    func closeInputViewDelegate() {
        if self.editViewHeight.constant != 0 {
            UIView.animate(withDuration: 0.5) {
                self.editViewHeight.constant = 0
                self.editView.setNeedsLayout()
                self.editView.layoutIfNeeded()
            } completion: { [weak self] finish in
                self?.editView.isHidden = true
            }
        }
        self.mask.isHidden = true
    }
}

//  MARK: - extension 實作Closure相關
extension DetailViewController{
    /**
     實作presentDatePickerClosure：把畫面present到DatePicker畫面
     */
    func presentDatePickerClosureImpl(date: String?){
        let datePickerVC = UIStoryboard(name: "DatePickerViewController", bundle: nil).instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        datePickerVC.date = self.editView.dateBtn.currentTitle
        self.present(datePickerVC, animated: true)

        //將datapicker取的值放回日期btn的closure
        datePickerVC.returnDate = { [weak self] returnDate in
            guard let selfVC = self else { return }
            if let date = returnDate {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd"
                selfVC.editView.dateBtn.setTitle(formatter.string(from: date), for: .normal)
                
            }
        }
    }
    /**
     實作closeEditViewClosure：關閉editView
     */
    func closeEditViewClosureImpl(){
        self.mask.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.editViewHeight.constant = 0
            self.mask.isHidden = true
            self.editView.setNeedsLayout()
            self.editView.layoutIfNeeded()
        } completion: { [weak self] finish in
            self?.editView.isHidden = true
        }
    }
    /**
     實作addDataClosure：新增資料
     */
    func addDataClosureImpl(_ newData: CellDataParam){
        if(newData.price != "0"){
            //新增detail資料
            let newDetailData = NSEntityDescription.insertNewObject(forEntityName: "DataInfoDetail", into: context ) as! DataInfoDetail
            if let itemText = newData.item{
                newDetailData.item = itemText
            }
            if let price = newData.price, let priceData = Int32(price){
                newDetailData.price = priceData
            }
            if let remark = newData.remark{
                newDetailData.remark = remark
            }
            if let itemImg = newData.itemImg{
                newDetailData.itemImg = itemImg
            }
            if let date = newData.date{
                newDetailData.date = date
            }
            
            //查詢此日期是否存在
            let dataInfo = queryDateIsExist(date: newData.date ?? "")
            //存在：新增detail   不存在：新增dataInfo + detail
            addDataDetail(dataInfo: dataInfo, newDetail: newDetailData)
            
            
            //editView下縮
//            self.closeInputViewDelegate()   //delegate寫法
            self.closeEditViewClosureImpl()
            
            //重載coredata資料
            self.getOriginCoreDataList()
            self.setOriginDataToUIData()
            dataTableView.reloadData()
            self.editView.reload()
        }
        else{
            //跳出alert告知失敗
            let alertController = UIAlertController(title: "Oops! 新增失敗", message: "請輸入正確資訊", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(okAction)
            present(alertController, animated: true)
            
        }
        
        
        //測試用1 Start--- print DataInfo+DataInfoDetail
//        self.getOriginCoreDataList()
//        if let dataInfoList = originDataList{
//            for dataInfo in dataInfoList{
//                print("dataInfo date: \(dataInfo.date ?? "沒有資料")")
//                print("dataInfo totalPrice: \(dataInfo.totalPrice)")
//                if let dataDetailList = (dataInfo.dataInfoDetail?.allObjects) as? [DataInfoDetail]{
//                    for detailInfo in dataDetailList{
//                        print("dataInfoDetail item: \(detailInfo.item ?? "沒有資料")")
//                        print("dataInfoDetail price: \(detailInfo.price)")
//                        print("dataInfoDetail remark: \(detailInfo.remark ?? "沒有資料")")
//                    }
//                }
//
//            }
//            print("測試用１ done")
//        }
        //測試用1 End
        
        //測試用2 Start--- core data轉為CellInfo格式 再print
//        self.getOriginCoreDataList()
//        self.setOriginDataToUIData()
//        print(cellUIDataList?.count)
//        if let datalist = self.cellUIDataList{
//            for UIData in datalist{
//                print("dataInfo date: \(UIData.date ?? "沒有資料")")
//                print("dataInfo totalPrice: \(UIData.totalPrice ?? "0")")
//                if let UIDataDetail = UIData.detailData{
//                    for detailData in UIDataDetail{
//                        print("dataInfoDetail item: \(detailData.item ?? "沒有資料")")
//                        print("dataInfoDetail price: \(detailData.price ?? "0")")
//                        print("dataInfoDetail remark: \(detailData.remark ?? "沒有資料")")
//                    }
//                }
//            }
//            print("測試用２ done")
//        }
        //測試用2 End
    }
    /**
    實作updateDataClosure：更新資料
     */
    func updateDataClosureImpl(_ newData: CellDataParam, objectId: NSManagedObjectID){
        if(Int(newData.price ?? "0") != 0){
            //先把原有的totalPrice扣掉修改前的金額並存
            if let detail = self.getAssignOriginCoreData(objectId: objectId) as? DataInfoDetail{
                //origin datainfo objectID
                let oriDataInfo = self.getAssignOriginCoreData(objectId: detail.dataInfo?.objectID) as? DataInfo
                detail.dataInfo?.totalPrice = (detail.dataInfo?.totalPrice ?? 0) - detail.price
                detail.dataInfo?.removeFromDataInfoDetail(detail)
                appDelegate.saveContext()
                
                //修改為新資料
                detail.item = newData.item
                detail.itemImg = newData.itemImg
                detail.remark = newData.remark
                detail.price = Int32(newData.price ?? "0") ?? 0
                detail.date = newData.date
                
                //查詢新日期是否存在
                let dataInfo = queryDateIsExist(date: newData.date ?? "")
                //存在：新增detail   不存在：新增dataInfo + detail
                addDataDetail(dataInfo: dataInfo, newDetail: detail)
                
                if let oriInfo = oriDataInfo, let dataInfoCount = oriInfo.dataInfoDetail?.count {
                    if(dataInfoCount == 0){
                        self.context.delete(oriInfo)
                    }
                }
            }
            appDelegate.saveContext()
            
            //editView下縮
//            self.closeInputViewDelegate()   //delegate寫法
            self.closeEditViewClosureImpl()
            
            //重新reload table view
            self.getOriginCoreDataList()
            self.setOriginDataToUIData()
            dataTableView.reloadData()
            
            editView.cellObjectId = nil
            self.editView.reload()
            
        }
        else{
            //跳出alert告知失敗
            let alertController = UIAlertController(title: "Oops! 更新失敗", message: "金額不得為０", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(okAction)
            present(alertController, animated: true)
        }
        
//        //將會被修改的detail跟DataInfo
//        let detailObj = self.getAssignOriginCoreData(objectId: objectId) as? DataInfoDetail
//        let dataInfoObj = detailObj?.dataInfo
//        
//        if let newDetail = detailObj{
//            newDetail.item = newData.item
//            newDetail.itemImg = newData.itemImg
//            newDetail.remark = newData.remark
//            newDetail.price = Int32(newData.price ?? "0") ?? 0
//
//            if(dataInfoObj?.date != newData.date){
//                //查詢新日期是否存在
//                var dataInfo = queryDateIsExist(date: newData.date ?? "")
//                //存在：新增detail   不存在：新增dataInfo + detail
//                addDataDetail(dataInfo: dataInfo, newDetail: newDetail)
//            }
//        }
//        
//        
//        
//        
//        
//        
//        if let detailObj = self.getAssignOriginCoreData(objectId: objectId) as? DataInfoDetail,
//            let originPrice = originDetail?.price,
//           let newPrice = Int32(newData.price ?? "0"){
//            //super class資訊
//            let originDataInfo = detailObj.dataInfo
//            
//            originDataInfo?.totalPrice = (originDataInfo?.totalPrice ?? 0) - originPrice + newPrice
//            detailObj.price = newPrice
//            detailObj.item = newData.item
//            detailObj.itemImg = newData.itemImg
//            detailObj.remark = newData.remark
//            
//            if(originDataInfo?.date != newData.date){
//                //查詢新日期是否存在
//                var dataInfo = queryDateIsExist(date: newData.date ?? "")
//                //存在：新增detail   不存在：新增dataInfo + detail
//                addDataDetail(dataInfo: dataInfo, newDetail: detailObj)
//            }
//        }
//        else{
//            //跳出alert告知失敗
//            let alertController = UIAlertController(title: "Oops! 更新失敗", message: "金額不得為０", preferredStyle: .alert)
//               let okAction = UIAlertAction(title: "OK", style: .default)
//               alertController.addAction(okAction)
//               present(alertController, animated: true)
//        }
    }
    
    /**
     讀取coredata有沒有同日期
     */
    func queryDateIsExist(date: String) -> DataInfo?{
        var dataInfo:DataInfo?
        //
        do {
            let fetchRequest : NSFetchRequest<DataInfo> = DataInfo.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "date == %@", date)
            dataInfo = try context.fetch(fetchRequest).first
            return dataInfo
        }catch{
            fatalError("Failed to fetch data: \(error)")
        }
    }
    
    /**
     如果同日期有資料,detail增加此筆資料,總金額加上此筆detail的金額
     沒有同日期就新增日期
     */
    func addDataDetail(dataInfo: DataInfo?, newDetail: DataInfoDetail){
        //如果同日期有資料,detail增加此筆資料,總金額加上此筆detail的金額
        if let dataInfo = dataInfo{
            newDetail.dataInfo = dataInfo
            dataInfo.totalPrice = dataInfo.totalPrice + newDetail.price
            dataInfo.addToDataInfoDetail(newDetail)
            
            
        }
        //如果沒有這個日期
        else{
            let newDataInfo = NSEntityDescription.insertNewObject(forEntityName: "DataInfo", into: context ) as? DataInfo
            if let date = newDetail.date{
                newDataInfo?.date = date
            }
            newDataInfo?.totalPrice = (newDataInfo?.totalPrice ?? 0) + newDetail.price
            newDataInfo?.addToDataInfoDetail(newDetail)
            newDetail.dataInfo = newDataInfo
            
        }
        appDelegate.saveContext()
    }
    
    /**
    push到EditTags頁面
     */
    func pushEditTagsVCClosureImpl(){
        let sb = UIStoryboard(name: "EditTagsViewController", bundle: nil)
        let editTagsVC = sb.instantiateViewController(withIdentifier: "EditTagsViewController") as! EditTagsViewController
        editTagsVC.reloadEditViewClosure = { self.reloadEditViewClosureImpl() }
        navigationController?.pushViewController(editTagsVC, animated: true)
    }
    
    /**
     新增／刪除標籤回到此頁面後reload editView裡面的tableView
     */
    func reloadEditViewClosureImpl(){
        self.editView.getItemTags()
    }
}
