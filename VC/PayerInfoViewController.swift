//
//  PayerInfoViewController.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/30.
//

import UIKit
/**
 付款狀態頁面
 */
class PayerInfoViewController: UIViewController {
    
    @IBOutlet weak var dataTableView: UITableView!
    var detailUid: String?
    var originLedgerDetails: FirebaseLedgerDetails?
    var cellUIData: [AnyObject]? = []
    override func viewDidLoad() {
        super.viewDidLoad()
        Task{
            await getLedgersDetail()
        }
        setDelegates()
        setFrames()
    }
    
    func setDelegates(){
        let nib = UINib(nibName: "PayerInfoTableViewCell", bundle: nil)
        self.dataTableView.register(nib, forCellReuseIdentifier: "PayerInfoTableViewCell")
        self.dataTableView.delegate = self
        self.dataTableView.dataSource = self
    }
    
    func setFrames(){
        self.dataTableView.layer.cornerRadius = 25
    }
    /**
     DB取得付款者資訊
     */
    func getLedgersDetail() async{
        if let db = dbParent, let detailUid{
            let docRef = db.collection("BillDetails").document(detailUid)
            do{
                //DB取資料
                let document = try await docRef.getDocument()
                //document如果存在
                if document.exists{
                    //將資料轉進自定義的class
                    if let ledgerDetail = try? document.data(as: FirebaseLedgerDetails.self){
                        self.originLedgerDetails = ledgerDetail
                        self.originListToCellUIData()
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
        }
        
    }
    
    func originListToCellUIData(){
        if let originLedgerDetails, let originPayerList = originLedgerDetails.payerList, originPayerList.count > 0{
            self.cellUIData = []
            for payer in originPayerList{
                let changeStatusClosure: ()->() = { [weak self] in
                    guard let strongSelf = self else { return }
                    let alertController = UIAlertController(title: nil, message: "確認修改付款狀態？", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "確認", style: .default) { [weak self] _ in
                        guard let selfVC = self, let payStatus = payer.payStatus else { return }
                        payer.payStatus = !payStatus
                        if let db = dbParent, let detailUid = selfVC.detailUid{
                            try? db.collection("BillDetails").document(detailUid).setData(from: originLedgerDetails.self, merge: true)
                        }
                        Task{
                            await selfVC.getLedgersDetail()	
                        }
                    }
                    let cancelAction = UIAlertAction(title: "取消", style: .cancel)
                    alertController.addAction(okAction)
                    alertController.addAction(cancelAction)
                    strongSelf.present(alertController, animated: true)
                }
                var data = PayerInfoTableViewCellData()
                if originLedgerDetails.creatorEmail == CurrentUserDBInfo.shareInstance.myInfo?.account{
                    if payer.payStatus == true{
                        data = PayerInfoTableViewCellData(userName: payer.payerName, userEmail: payer.payerEmail, amount: payer.pay, payStatus: payer.payStatus, payStatusTextColor: payStatusTrue, payStatusBorderColor: cgPayStatusTrue, hasChangeStatusBtn: true, changeStatusClosure: changeStatusClosure)
                    }
                    else{
                        data = PayerInfoTableViewCellData(userName: payer.payerName, userEmail: payer.payerEmail, amount: payer.pay, payStatus: payer.payStatus, payStatusTextColor: payStatusFalse, payStatusBorderColor: cgPayStatusFalse, hasChangeStatusBtn: true, changeStatusClosure: changeStatusClosure)
                    }
                }
                else{
                    if payer.payStatus == true{
                        data = PayerInfoTableViewCellData(userName: payer.payerName, userEmail: payer.payerEmail, amount: payer.pay, payStatus: payer.payStatus, payStatusTextColor: payStatusTrue, payStatusBorderColor: cgPayStatusTrue, hasChangeStatusBtn: false)
                    }
                    else{
                        data = PayerInfoTableViewCellData(userName: payer.payerName, userEmail: payer.payerEmail, amount: payer.pay, payStatus: payer.payStatus, payStatusTextColor: payStatusFalse, payStatusBorderColor: cgPayStatusFalse, hasChangeStatusBtn: false)
                    }
                }
                self.cellUIData?.append(data)
            }
        }
        self.dataTableView.reloadData()
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PayerInfoViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellUIData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let tableViewData = cellUIData?[indexPath.row] {
            switch tableViewData {
            case let cellUIData as PayerInfoTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PayerInfoTableViewCell", for: indexPath) as? PayerInfoTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            default:
                break
            }
        }
        return cell
    }
}
