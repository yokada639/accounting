//
//  EditTagsViewController.swift
//  記帳
//
//  Created by Murphy on 2024/5/5.
//

import UIKit

class EditTagsViewController: UIViewController {
    private var userDefaults = UserDefaults.standard
    var currentTags:[ItemTagModel]?
    var reloadEditViewClosure: ()->() = {}
    @IBOutlet weak var dataTableView: UITableView!
    
    @IBOutlet weak var btnMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.setDelegateSettings()
        self.getTagList()
        self.initMenuBtn()
        self.navigationItem.title = "標籤"
        
    }
    
//  MARK: - IBActions
    
    
    /**
     btn-回上一頁：回detail頁面
     */
    @IBAction func backToLastPage(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = false
        self.reloadEditViewClosure()
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Functions
    /**
     delegate相關設定
     */
    func setDelegateSettings(){
        let nib = UINib(nibName: "EditTagsTableViewCell", bundle: nil)
        dataTableView.register(nib, forCellReuseIdentifier: "EditTagsTableViewCell")
        dataTableView.delegate = self
        dataTableView.dataSource = self
        dataTableView.dragDelegate = self
        dataTableView.dragInteractionEnabled = true
    }
    
    /**
     從user defaults取得tag list
     */
    func getTagList(){
        if let list = userDefaults.object(forKey: "itemTagList") as? Data {
            let decoder = JSONDecoder()
            self.currentTags = try? decoder.decode([ItemTagModel].self, from: list)
        }
    }
    
    /**
        初始化右上角選單(menu)內容
     */
    func initMenuBtn(){
        btnMenu.showsMenuAsPrimaryAction = true
        btnMenu.menu = UIMenu(children: [
            UIAction(title: "新增", image: UIImage(named: "add-48"), handler: { [weak self] UIAction in
                    self?.addNewTag()
                }),
                UIAction(title: "全部重置", image: UIImage(named: "reset-48"), handler: { [weak self] UIAction in
                    self?.resetTags()
                    self?.getTagList()
                    self?.dataTableView.reloadData()
                    self?.dataTableView.layoutIfNeeded() //強制立刻更新佈局
                    
                    let indexPath = IndexPath(row: 0, section: 0)
                    self?.dataTableView.scrollToRow(at: indexPath, at: .top, animated: true)
                })
        ])
    }
    
    /**
     btn-新增：present到新增頁面
     */
    func addNewTag() {
        let addNewTagVC = UIStoryboard(name: "AddNewTagViewController", bundle: nil).instantiateViewController(withIdentifier: "AddNewTagViewController") as! AddNewTagViewController
        addNewTagVC.currentTags = self.currentTags
        addNewTagVC.reloadDataTableviewClosure = { self.reloadDataTableviewClosureImpl() }
//        addNewTagVC.modalPresentationStyle = .overFullScreen
//        addNewTagVC.view.backgroundColor = .clear
        self.present(addNewTagVC, animated: true)
    }
    
    /**
    btn-重置tag陣列
     */
    func resetTags(){
        let tagModel = ItemTagModel()
        tagModel.initArrayInfo()
        saveTagListToUserDefaults(tagList: tagModel.tagList, keyName: "itemTagList")
        
    }
    
    /**
     將tag陣列存到user defaults
     */
    /// - parameter tagList: tag陣列
    /// - parameter keyName: user defaults的key值、
    func saveTagListToUserDefaults(tagList: [ItemTagModel]?, keyName: String){
        if let list = tagList{
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(list) {
                self.userDefaults.set(encoded, forKey: keyName)
            }
        }
    }
}

// MARK: - extension UITableViewDelegate, UITableViewDataSource, UITableViewDragDelegate
extension EditTagsViewController: UITableViewDelegate, UITableViewDataSource, UITableViewDragDelegate{
    /**
     UITableViewDragDelegate 拖曳cell的delegate
     */
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let dragItem = UIDragItem(itemProvider: NSItemProvider())
            dragItem.localObject = currentTags?[indexPath.row]
        return [ dragItem ]
    }
    
    
    /**
     return cell數量
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentTags?.count ?? 0
    }
    
    /**
     設定cell內容
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditTagsTableViewCell", for: indexPath) as! EditTagsTableViewCell
        if let tag = currentTags?[indexPath.row]{
            cell.setCell(tag: tag)
        }
        return cell
    }
    
    /**
    cell左滑刪除
     */
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //設定刪除Action（cell左滑）
        let deleteAction = UIContextualAction(style: .destructive, title: "刪除") { [weak self] (action, view, completion) in
            //撈出來的tag list中刪除符合的元件，再存進userDefaults + tableView reload
            if var tagList = self?.currentTags{
                tagList.remove(at: indexPath.row)
                self?.saveTagListToUserDefaults(tagList: tagList, keyName: "itemTagList")
            }
            self?.getTagList()
            self?.dataTableView.reloadData()
        }
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])

        return configuration
    }
    
    /**
     可以設定indexPath在哪幾格才可以被移動
     */
//    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
    
    /**
     移動tag list時改變資料排序
     moveRowAt:取得移動前跟移動後的index
     */
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let beforeIndex = sourceIndexPath.row
        let afterIndex = destinationIndexPath.row
        if let tag = currentTags?[beforeIndex]{
            self.currentTags?.remove(at: beforeIndex)
            self.currentTags?.insert(tag, at: afterIndex)
            saveTagListToUserDefaults(tagList: self.currentTags, keyName: "itemTagList")
        }
    }
}

//  MARK: - Closure 實作
extension EditTagsViewController{
    /**
     實作reloadDataTableviewClosure：重載table view Data及畫面
     */
    func reloadDataTableviewClosureImpl(){
        self.getTagList()
        self.dataTableView.reloadData()

        //MARK: 因為reloadData是異步執行，所以讓UI到主執行序進行更動
        DispatchQueue.main.async {
            if let tagListCount = self.currentTags?.count{
                //設定indexPath(由0開始)，並用scrollToRow將tableView移到該indexPath
                let indexPath = IndexPath(row: tagListCount - 1, section: 0)
                self.dataTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
        /*第二種寫法：reloadData後強制立刻更新佈局，再去做後續動作
         self.getTagList()
         self.dataTableView.reloadData()
         self.dataTableView.layoutIfNeeded()
            ....
        */
    }
}
