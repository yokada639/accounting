//
//  PermissionViewController.swift
//  Ledgers
//
//  Created by Murphy on 2024/8/21.
//

import UIKit

class PermissionViewController: UIViewController {

    @IBOutlet weak var dataTableView: UITableView!
    
    var ledgerId: String?
    var originSharedUsersUid: [String]?
    var originSharedUserInfo: [FirebaseUserInfo]?
    var cellUIData: [AnyObject]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Task{
            ProgressTool.shareInstance.progressStart()
            await self.getUserApi()
//            if let originSharedUsdrInfo{
//                for user in originSharedUsdrInfo{
//                    print(user.account)
//                }
//            }
        }
        self.setUILayer()
        self.setDelegates()
        // Do any additional setup after loading the view.
    }
    
    func setUILayer(){
        self.navigationItem.title = "共享者管理"
        self.dataTableView.layer.cornerRadius = 25
    }
    
    func setDelegates(){
        let nib = UINib(nibName: "PayUsersDetailTableViewCell", bundle: nil)
        self.dataTableView.register(nib, forCellReuseIdentifier: "PayUsersDetailTableViewCell")
        self.dataTableView.delegate = self
        self.dataTableView.dataSource = self
    }
    //從db取得此帳本共享者清單
    func getUserApi() async{
        self.originSharedUsersUid = []
        self.originSharedUserInfo = []
        if let db = dbParent, let ledgerId{
            do{
                let docRef = db.collection("Ledger").document(ledgerId)
                //DB取資料
                let document = try await docRef.getDocument()
                //document如果存在
                if document.exists{
                    //將資料轉進自定義的class
                    if let ledgerInfo = try? document.data(as: FirebaseLedgerInfo.self){
                        self.originSharedUsersUid = ledgerInfo.sharedUsers
                        if let originSharedUsersUid{
                            for userUid in originSharedUsersUid{
                                let docRef = db.collection("User").document(userUid)
                                let userDoc = try await docRef.getDocument()
                                if userDoc.exists{
                                    if let userInfo = try? userDoc.data(as: FirebaseUserInfo.self){
                                        self.originSharedUserInfo?.append(userInfo)
                                    }
                                }
                            }
                            self.setUIData()
                        }
                        
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
        }
        ProgressTool.shareInstance.progressEnd()
    }

    //將資料轉為UI用Data
    func setUIData(){
        self.cellUIData = []
        if let originSharedUserInfo, originSharedUserInfo.count > 0 {
            let removeUserClosure: (_ : PayUsersDetailTableViewCellData?) -> () = { [weak self] user in
                guard let strongSelf = self else { return }
                let alertController = UIAlertController(title: "是否移除此帳號?", message: "【重要】\r\n此動作無法復原", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "確定", style: .default) { [weak self] _ in
                    guard let selfVC = self else { return }
                    Task{
                        await selfVC.removeUser(user: user)
                    }
                    let doneAlertController = UIAlertController(title: nil, message: "移除成功", preferredStyle: .alert)
                    let confirmedAction = UIAlertAction(title: "確定", style: .default) { [weak self] _ in
                        guard let selfVC = self else { return }
                        Task{
                            await selfVC.getUserApi()
                        }
                    }
                    doneAlertController.addAction(confirmedAction)
                    selfVC.present(doneAlertController, animated: true)
                }
                alertController.addAction(okAction)
                let cancelAction = UIAlertAction(title: "取消", style: .cancel)
                alertController.addAction(cancelAction)
                
                strongSelf.present(alertController, animated: true)
            }
            for userInfo in originSharedUserInfo{
                if userInfo.uid != CurrentUserDBInfo.shareInstance.myInfo?.uid{
                    let data = PayUsersDetailTableViewCellData(uid: userInfo.uid, userName: userInfo.userName, userEmail: userInfo.account, removeUserClosure: removeUserClosure, moneyIsHidden: true, imageName: "tools_do-not-disturb-60")
                    self.cellUIData?.append(data)
                }
            }
        }
        self.dataTableView.reloadData()
    }
    
    /**
     刪除帳本內指定共享者
     刪除BillDetail中該帳本payerList含user uid的帳務
     刪除Ledger中該帳本sharedUsers內的uid
     刪除User中invitedLedgers內的uid
     */
    func removeUser(user: PayUsersDetailTableViewCellData?) async {
        if let db = dbParent, let ledgerId, let user,  let userId = user.uid{
            //刪除BillDetail中該帳本payerList含user uid的帳務
            var docRef = db.collection("BillDetails").whereField("ledgerID", isEqualTo: ledgerId)
            docRef.getDocuments { [weak self] snapshot, error in
                guard let snapshot else { return }
                if !snapshot.documents.isEmpty{
                    let billDetails = snapshot.documents.compactMap { [weak self] dataSnapshot in
                        try? dataSnapshot.data(as: FirebaseLedgerDetails.self)
                    }
                    for detail in billDetails{
                        if let payerIndex = detail.payerList?.firstIndex(where: {$0.payerID == userId}){
                            detail.payerList?.remove(at: payerIndex)
                        }
                        try? db.collection("BillDetails").document(detail.uid ?? "").setData(from: detail.self, merge: true)
                    }
                }
            }
            
            //刪除Ledger中該帳本sharedUsers內的uid
            docRef = db.collection("Ledger").whereField("ledgerId", isEqualTo: ledgerId)
            docRef.getDocuments { [weak self] snapshot, error in
                guard let snapshot else { return }
                if !snapshot.documents.isEmpty{
                    let ledgerInfo = snapshot.documents.compactMap { [weak self] dataSnapshot in
                        try? dataSnapshot.data(as: FirebaseLedgerInfo.self)
                    }
                    for info in ledgerInfo{
                        if let userIndex = info.sharedUsers?.firstIndex(where: {$0 == userId}){
                            info.sharedUsers?.remove(at: userIndex)
                        }
                        try? db.collection("Ledger").document(ledgerId).setData(from: info.self, merge: true)
                    }
                }
            }
            
            //刪除User中invitedLedgers內的uid
            let ref = db.collection("User").document(userId)
            do{
                let document = try await ref.getDocument()
                if document.exists{
                    if let userInfo = try? document.data(as: FirebaseUserInfo.self), let index = userInfo.invitedLedgers?.firstIndex(where: {$0 == ledgerId}){
                        userInfo.invitedLedgers?.remove(at: index)
                        try? ref.setData(from: userInfo.self, merge: true)
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    /**
     email查詢firebase內邀請的使用者資訊
     query回傳有可能為多個結果(此處為單一結果），要用getDocuments去接
     */
    func queryInviteUser(email: String) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        if let db = dbParent{
            let docRef = db.collection("User").whereField("account", isEqualTo: email).whereField("enable", isEqualTo: true)
            docRef.getDocuments { [weak self] snapshot, error in
                guard let snapshot, let strongSelf = self else { return }
                if snapshot.documents.isEmpty{
                    alertController.message = "查無使用者"
                }
                else{
                    //snapshot.documents.compactMap{ ... } 將query的資料轉進自訂的class
                    let userList = snapshot.documents.compactMap{ [weak self] snapshot in
                        try? snapshot.data(as: FirebaseUserInfo.self)
                    }
                    for user in userList{
                        if let selfUid = userDefaults.string(forKey: "DBKey"), let userUid = user.uid, selfUid != userUid{
                            if let originSharedUsersUid = strongSelf.originSharedUsersUid, originSharedUsersUid.contains(userUid){
                                alertController.message = "該使用者已在此帳本內"
                            }
                            else{
                                strongSelf.sentInvite(user: user)
                                alertController.message = "邀請成功"
                            }
                        }
                        else{
                            alertController.message = "邀請對象不可為自己"
                        }
                    }
                }
                let okAction = UIAlertAction(title: "確定", style: .default)
                alertController.addAction(okAction)
                strongSelf.present(alertController, animated: true)
            }
        }
    }
    //發送邀請
    func sentInvite(user: FirebaseUserInfo){
        user.unconfirmedLedgers?.append(self.ledgerId ?? "")
        if let db = dbParent, let userUid = user.uid{
            try? db.collection("User").document(userUid).setData(from: user.self, merge: true)
        }
    }
    //返回上一頁
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //邀請新使用者
    @IBAction func inviteNewUser(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: "請輸入邀請對象E-mail", preferredStyle: .alert)
        //alert添加Textfield
        alertController.addTextField()
        let okAction = UIAlertAction(title: "確定", style: .default) { [weak self] _ in
            //用index取alertController上第一個textField: alertController.textFields?[0]
            guard let input = alertController.textFields?[0] else{ return }
            
            //讓Firebase去Query使用者
            self?.queryInviteUser(email: input.text ?? "")
        }
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true)
    }


}

extension PermissionViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellUIData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let tableViewData = cellUIData?[indexPath.row] {
            switch tableViewData {
            case let cellUIData as PayUsersDetailTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PayUsersDetailTableViewCell", for: indexPath) as? PayUsersDetailTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            default:
                break
            }
        }
        return cell
    }
    
    
}
