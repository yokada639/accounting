//
//  LedgerListViewController.swift
//  記帳
//
//  Created by Murphy on 2024/5/11.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class LedgerListViewController: UIViewController {
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var ledgerSegment: UISegmentedControl!
    
    @IBOutlet weak var ledgerCollectionView: UICollectionView!
    var invitedLedgerList: [FirebaseLedgerInfo]?
    var ownerledgerList: [FirebaseLedgerInfo]?
    var ledgerUIDataList: [AnyObject]? = []
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.backgroundView.layer.cornerRadius = 25
        self.navigationItem.title = "雲端共享帳本"
        setDelegates()
        initMenu()
        
        self.view.accessibilityIgnoresInvertColors = true

        
        
    }
    
    //  MARK: autolayout完成後才呼叫viewDidLayoutSubviews()
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureCellSize()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //MARK: 取當前user及帳本資訊
        FirebaseDocSettings.shareInstance.getcurrentUserAndLedgers(vc: self)
    }
    //MARK: - IBActions
    /**
     用segment controller去切換頁面顯示內容
     */
    @IBAction func pressSegment(_ sender: UISegmentedControl) {
        //index 0 = 我的共享帳本
        if ledgerSegment.selectedSegmentIndex == 0{
            Task{
                //MARK: 主執行緒：DispatchQueue.main.async { ... }
                ProgressTool.shareInstance.progressStart()
                await self.getOwnerLedgers()
                self.setUIData(ledgerList: self.ownerledgerList ?? [])
                ProgressTool.shareInstance.progressEnd()
            }
        }
        //index 1 = 受邀請的帳本
        else{
            Task{
                //MARK: 主執行緒：DispatchQueue.main.async { ... }
                ProgressTool.shareInstance.progressStart()
                await self.getInvitedLedgers {
                    self.setUIData(ledgerList: self.invitedLedgerList ?? [])
                }
                ProgressTool.shareInstance.progressEnd()
            }
        }
    }
    
    //MARK: - Functions
    /**
     設定Delegate
     */
    func setDelegates(){
        let nib = UINib(nibName: "LedgerListCollectionViewCell", bundle: nil)
        self.ledgerCollectionView.register(nib, forCellWithReuseIdentifier: "LedgerListCollectionViewCell")
        ledgerCollectionView.delegate = self
        ledgerCollectionView.dataSource = self
    }
    
    /**
     設定Collection View UI Data + reload
     */
    func setUIData(ledgerList: [FirebaseLedgerInfo]){
        //每次重新reload cell時 UIDataList要先初始化，不然資料會重複出現
        ledgerUIDataList = []
        if ledgerList.count > 0 {
            for ledger in ledgerList{
                let goToDetailClosure = { [weak self] in
                    guard let strongSelf = self else { return }
                    let sb = UIStoryboard(name: "SharedLedgerDetailViewController", bundle: nil)
                    let sharedLedgerVC = sb.instantiateViewController(withIdentifier: "SharedLedgerDetailViewController") as! SharedLedgerDetailViewController
                    sharedLedgerVC.ledgerId = ledger.ledgerId
                    sharedLedgerVC.ledgerOwnerId = ledger.ownerUid
                    strongSelf.navigationController?.pushViewController(sharedLedgerVC, animated: true)
                }
                let data = LedgerListCollectionViewCellData(ledgerName: ledger.ledgerName, goToledgerDetailClosure: goToDetailClosure)
                
                self.ledgerUIDataList?.append(data)
            }
            self.ledgerCollectionView.reloadData()
        }
    }
    
    
    /**
     調整collection view的cell大小
     要調整cell的間距，一定要同時調整CollectionView UI內的Collection View Flow Layout
     參數：min spacing: cell之間的間距 /section inset: 與collection view 上下左右的間距
     */
    func configureCellSize() {
        //section上下左右30, min spacing 20*1=20 合計80
        let width = (self.ledgerCollectionView.frame.width - 80.1) / 2
        let flowLayout = self.ledgerCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        flowLayout?.itemSize = CGSize(width: width, height: 100)
        flowLayout?.estimatedItemSize = .zero

        self.ledgerCollectionView.reloadData()
    }
    
    /**
     初始化menu選單
     */
    func initMenu(){
        menuButton.showsMenuAsPrimaryAction = true
        menuButton.menu = UIMenu(children: [
                UIAction(title: "新增共享帳本", 
                         image: UIImage(named: "add-60"),
                         handler: { [weak self] UIAction in
                             guard let selfVC = self else { return }
                             if let addNewLedgerVC = UIStoryboard(name: "AddNewLedgerViewController", bundle: nil).instantiateViewController(withIdentifier: "AddNewLedgerViewController") as? AddNewLedgerViewController{
                                 addNewLedgerVC.reloadCollectionViewClosure = { [weak self] in
                                     Task{
                                         ProgressTool.shareInstance.progressStart()
                                         await selfVC.getOwnerLedgers()
                                         //判斷segment controller的index，如果是在我的共享帳本頁面才去設定UIData
                                         if self?.ledgerSegment.selectedSegmentIndex == 0 {
                                             selfVC.setUIData(ledgerList: selfVC.ownerledgerList ?? [])
                                         }
                                         ProgressTool.shareInstance.progressEnd()
                                         
                                     }
                                 }
                                 addNewLedgerVC.modalPresentationStyle = .overFullScreen
                                 selfVC.present(addNewLedgerVC, animated: true)
                             }
                             
                         }
                ),
                UIAction(title: "待核准的邀請",
                         image: UIImage(named: "tools_unconfirmed-60"),
                         handler: { [weak self] UIAction in
                             guard let selfVC = self else { return }
                             if let unconfirmedVC = UIStoryboard(name: "UnconfirmedViewController", bundle: nil).instantiateViewController(withIdentifier: "UnconfirmedViewController") as? UnconfirmedViewController{
                                 
                                 //MARK: 棄用（重複呼叫Api）
/*                                 unconfirmedVC.reloadLastPageLedgers = { [weak self] in
                                     Task{
                                         ProgressHUD.animationType = .circleArcDotSpin
                                         ProgressHUD.colorAnimation = yellowBG
                                         ProgressHUD.animate("Loading...",interaction: false)
                                         await selfVC.getInvitedLedgers {
                                             判斷segment controller的index，如果是在受邀請的帳本頁面才去設定UIData
                                             if self?.ledgerSegment.selectedSegmentIndex == 1{
                                                 selfVC.setUIData(ledgerList: selfVC.invitedLedgerList ?? [])
                                             }
                                         }
                                         ProgressHUD.dismiss()
                                     }
                                 }  */
                                 
                                 self?.navigationController?.pushViewController(unconfirmedVC, animated: true)
                                 
                             }
                             
                         }
                ),
                //menu做分段(中間一條灰色)：陣列內放一個UIMenu實體
                UIMenu(options: .displayInline,
                       children: [/*UIAction(title: "修改個人資料",
                                           image: UIImage(named: "tools_edit-profile-60"),
                                           handler: { [weak self] UIAction in
                                               //TODO: 修改個人資料
                                           }
                                  ),*/
                                  UIAction(title: "登出",
                                           image: UIImage(named: "tool_sign-out-48"),
                                           handler: { [weak self] UIAction in
                                               do {
                                                   try Auth.auth().signOut()
                                                   self?.logoutInit()
                                               } catch {
                                                   print(error)
                                               }
                                           }
                                  ),
                                  //第一次送審：提供刪除帳號的方式
                                  UIAction(title: "刪除帳號",
                                           image: UIImage(named: "tools-delete-user-60"),
                                           handler: { [weak self] UIAction in
                                               guard let strongSelf = self else { return }
                                               
                                               let alertController = UIAlertController(title: "刪除帳號", message: "【注意】\r\n刪除帳號後所有資料將全部刪除", preferredStyle: .alert)
                                               
                                               let okAction = UIAlertAction(title: "OK", style: .default){ [weak self] _ in
                                                   guard let selfVC = self else { return }
                                                   let user = Auth.auth().currentUser
                                                   user?.delete(completion: { error in
                                                       if let error{
                                                           let errorAlert = UIAlertController( title: nil, message: "刪除失敗，請至https://murphyssupport.mystrikingly.com/留言告知您的問題", preferredStyle: .alert)
                                                           let okAct = UIAlertAction(title: "OK", style: .default)
                                                           errorAlert.addAction(okAct)
                                                           selfVC.present(errorAlert, animated: true)
                                                       }
                                                       else{
                                                           Task{
                                                               await selfVC.setUserDisable(user: user)
                                                           }
                                                           let deleteAlert = UIAlertController( title: nil, message: "您的帳號已成功刪除", preferredStyle: .alert)
                                                           let okAct = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
                                                               selfVC.tabBarController?.selectedIndex = 0
                                                        
                                                           }
                                                           deleteAlert.addAction(okAct)
                                                           selfVC.present(deleteAlert, animated: true)
                                                       }
                                                       
                                                   })
                                               }
                                               alertController.addAction(okAction)
                                               
                                               let cancelAction = UIAlertAction(title: "取消", style: .cancel)
                                               alertController.addAction(cancelAction)
                                               
                                               strongSelf.present(alertController, animated: true)
                                           }
                                  )
                ])
        ])
    }
    /**
        把db User enable改為false
     */
    func setUserDisable(user: User?) async{
        if let db = dbParent, let uid = user?.uid{
            let docRef = db.collection("User").document(uid)
            do{
                let document = try await docRef.getDocument()
                if document.exists{
                    if let userInfo = try? document.data(as: FirebaseUserInfo.self){
                        userInfo.enable = false
                        try? db.collection("User").document(uid).setData(from: userInfo, merge: true)
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    /**
     登出後將資料及畫面初始化
     */
    func logoutInit(){
        userDefaults.removeObject(forKey: "DBKey")
        self.ledgerUIDataList = [] //清掉collectionView的資料
        self.ledgerCollectionView.reloadData() //重新整理collectionView
        self.ledgerSegment.selectedSegmentIndex = 0 //segment controller復歸到我的共享帳本
        self.tabBarController?.selectedIndex = 0 //tab bar自動跳轉到我的帳本
    }
    
    /**
     DB取得自己帳本明細
     */
    func getOwnerLedgers() async {
        self.ownerledgerList = [FirebaseLedgerInfo]()
        if let ownedLedgers = CurrentUserDBInfo.shareInstance.myInfo?.ownedLedgers,
           let db = dbParent{
            for ledgerID in ownedLedgers{
                do{
                    //MARK: docRef：設定路徑
                    let docRef = db.collection("Ledger").document(ledgerID)
                    let document = try await docRef.getDocument()
                    if document.exists{
                        if let ledgerInfo = try? document.data(as: FirebaseLedgerInfo.self){
                            self.ownerledgerList?.append(ledgerInfo)
                        }
                    }
                    else{
//                        self.errorSignOut()
                    }
                }
                catch{
                    print("Error getting document: \(error)")
                }
            }
            self.ledgerCollectionView.reloadData()
        }
    }
    
    /**
     DB取得被邀請的帳本明細
     */
    func getInvitedLedgers(complete: @escaping () -> Void = {}) async {
        self.invitedLedgerList = [FirebaseLedgerInfo]()
        if let invitedLedgers = CurrentUserDBInfo.shareInstance.myInfo?.invitedLedgers,
           let db = dbParent{
            for ledgerID in invitedLedgers{
                //MARK: docRef：設定路徑
                let docRef = db.collection("Ledger").document(ledgerID)
                do{
                    let document = try await docRef.getDocument()
                    if document.exists{
                        if let ledgerInfo = try? document.data(as: FirebaseLedgerInfo.self){
                            self.invitedLedgerList?.append(ledgerInfo)
                        }
                    }
                    else{
//                        self.errorSignOut()
                    }
                }
                catch{
                    print("Error getting document: \(error)")
                }
            }
            self.ledgerCollectionView.reloadData()
            complete()
        }
    }
    
    /**
     取資料失敗後登出
     */
    func errorSignOut(){
        let alertController = UIAlertController(title: nil, message: "資料處理失敗，請重新登入", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
            self?.dismiss(animated: true)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
        do{
            try Auth.auth().signOut()
            self.logoutInit()
        }
        catch{
            print("Error getting document: \(error)")
        }
        
    }
}

//MARK: - extension UICollectionViewDelegate, UICollectionViewDataSource
extension LedgerListViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ledgerUIDataList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        if let ledgerUIDataList = self.ledgerUIDataList, ledgerUIDataList.count > 0{
            if let collectionViewData = self.ledgerUIDataList?[indexPath.row]{
                //根據取出來的data去確認資料class
                switch collectionViewData {
                case let cellUIData as LedgerListCollectionViewCellData:
                    if let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LedgerListCollectionViewCell", for: indexPath) as? LedgerListCollectionViewCell{
                        collectionViewCell.setupCell(data: cellUIData)
                        cell = collectionViewCell
                    }
                default:
                    break
                }
                
            }
        }
        
        return cell
    }
}
