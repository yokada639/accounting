//
//  BillDetailViewController.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/4.
//

import UIKit
/**
 新增帳務頁面
 */
class BillDetailViewController: UIViewController {
    @IBOutlet weak var dataTableView: UITableView!
    
    //tabview
    var cellUIData: [AnyObject]? = []
    var billTitle: String?
    var billAmount: String?
    var billRemark: String?
    
    //從帳本明細頁頁面SharedLedgerDetailViewController 傳過來
    var ledgerId: String?
    
    //singleton取得User List
    var originSharedUserList: [FirebaseUserInfo]? = []
    
    //原始Cell UI User List
    var sharedUsersCellUIData: [PayUsersDetailTableViewCellData]? = []

    var reloadSharedLedgerTableViewClosure: () -> () = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.originSharedUserList = CurrentLedgerSharedUser.shareInstance.users
        setUserListToUIData()
        
        setUILayer()
        setDelegate()
        cellSettings()
    }
    
    /**
     點空白處收鍵盤（碰到螢幕時觸發）
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK: - Functions
    func setUILayer(){
        self.navigationItem.title = "新增帳務"
        self.dataTableView.layer.cornerRadius = 25
    }

    func setDelegate(){
        let billInfoNib = UINib(nibName: "BillInfoTableViewCell", bundle: nil)
        self.dataTableView.register(billInfoNib, forCellReuseIdentifier: "BillInfoTableViewCell")
        let addUserNib = UINib(nibName: "AddedUserTableViewCell", bundle: nil)
        self.dataTableView.register(addUserNib, forCellReuseIdentifier: "AddedUserTableViewCell")
        let payUserNib = UINib(nibName: "PayUsersTableViewCell", bundle: nil)
        self.dataTableView.register(payUserNib, forCellReuseIdentifier: "PayUsersTableViewCell")
        
        self.dataTableView.delegate = self
        self.dataTableView.dataSource = self
    }
    
    /**
     tableView Cell設定
     cell 1: 標題／總金額／備註
     cell 2: push到添加分帳者頁面
     cell 3: 被新增者資訊
     */
    func cellSettings(){
        // MARK: - 標題／總金額／備註 cell設定
        //帳本標題closure
        let titleClosure = { [weak self] title in
            guard let strongSelf = self else { return }
            strongSelf.billTitle = title
        }
        //帳本金額closure
        let amountClosure = { [weak self] amount in
            guard let strongSelf = self else { return }
            strongSelf.billAmount = amount
        }
        //帳本備註closure
        let remarkClosure = { [weak self] remark in
            guard let strongSelf = self else { return }
            strongSelf.billRemark = remark
        }
        let titleCellData = BillInfoTableViewCellData(getTitleClosure: titleClosure, getAmountClosure: amountClosure, getRemarkClosure: remarkClosure)
        self.cellUIData?.append(titleCellData)
        
        
        //MARK: -push到添加分帳者頁面cell
        let addUserBtnClosure = { [weak self] in
            guard let strongSelf = self else { return }
            let sb = UIStoryboard(name: "AddSharedUserViewController", bundle: nil)
            let vc = sb.instantiateViewController(identifier: "AddSharedUserViewController") as! AddSharedUserViewController
            vc.userList = strongSelf.sharedUsersCellUIData?.filter({ $0.payUserState == 0 }) ?? []
            vc.updateUserInfoClosure = { [weak self] user in
                guard let strongSelf2 = self else { return }
                strongSelf2.updateSharedUserData(user: user)
            }
            strongSelf.navigationController?.pushViewController(vc, animated: true)
        }
        
        let addUserCellData = AddedUserTableViewCellData(addUserBtnClosure: addUserBtnClosure)
        self.cellUIData?.append(addUserCellData)
        
        
        //MARK: - 被新增者資訊cell
        let updateUserListClosure:(_ : PayUsersDetailTableViewCellData?) -> () = { [weak self] user in
            guard let strongSelf = self else { return }
            if let userIndex = strongSelf.sharedUsersCellUIData?.firstIndex(where: {$0.userEmail == user?.userEmail}), let oriUser = strongSelf.sharedUsersCellUIData?[userIndex]{
                oriUser.payUserState = user?.payUserState
                oriUser.payMoney = user?.payMoney
            }
        }
        let editAmountClosure: (_ : IndexPath) -> () = { [weak self] index in
            guard let strongSelf = self else { return }
            let alertController = UIAlertController(title: nil, message: "欲修改的金額", preferredStyle: .alert)
            alertController.addTextField { [weak self] textfield in
                guard let strongSelf = self else {return}
                textfield.keyboardType = .numberPad
            }
                
            let sendAction = UIAlertAction(title: "送出", style: .default){ [weak self] _ in
                //用index取alertController上第一個textField: alertController.textFields?[0]
                guard let selfVC = self, let amount = alertController.textFields?[0].text else { return }
                let userList =  selfVC.sharedUsersCellUIData?.filter({ $0.payUserState == 1 }) ?? []
                
//                selfVC.classifyStatusesInUserUIData(sharedList: selfVC.sharedUsersCellUIData, state: 1)
                let user = userList[index.row]
                if amount.validateAmount(){
                    user.payMoney = amount
                   
                }
                else{
                    let errorAlertController = UIAlertController(title: nil, message: "不可輸入0及非數字字元", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "確認", style: .default)
                    errorAlertController.addAction(okAction)
                    selfVC.present(errorAlertController, animated: true)
                }
                strongSelf.dataTableView.reloadData()
                
                
            }
            let cancelAction = UIAlertAction(title: "取消", style: .cancel) { [weak self] _ in
                guard let selfVC = self else { return }
                strongSelf.dataTableView.reloadData()
            }
            
            alertController.addAction(sendAction)
            alertController.addAction(cancelAction)
            strongSelf.present(alertController, animated: true)
            
            
        }
        let payUserCellData = PayUsersTableViewCellData(userList: sharedUsersCellUIData?.filter({ $0.payUserState == 1 }) ?? [], updateUser: updateUserListClosure, editAmount: editAmountClosure)
        self.cellUIData?.append(payUserCellData)
        
        self.dataTableView.reloadData()

    }
    /**
     firebase所有使用者轉成UI Data
     */
    func setUserListToUIData(){
        if let originSharedUserList, originSharedUserList.count > 0{
            for user in originSharedUserList{
                let data = PayUsersDetailTableViewCellData(uid: user.uid, userName: user.userName, userEmail: user.account, payUserState: 0)
                self.sharedUsersCellUIData?.append(data)
            }
        }
    }
    
    /**
     根據指定使用者更新cellUIData內資料
     */
    func updateUserUIData(){
        let updateUserListClosure:(_ : PayUsersDetailTableViewCellData?) -> () = { [weak self] user in
            guard let strongSelf = self else { return }
            if let userIndex = strongSelf.sharedUsersCellUIData?.firstIndex(where: {$0.userEmail == user?.userEmail}), let oriUser = strongSelf.sharedUsersCellUIData?[userIndex]{
                oriUser.payUserState = user?.payUserState
                oriUser.payMoney = user?.payMoney
            }
            strongSelf.updateUserUIData()
        }
        var addedUsersList = self.classifyStatusesInUserUIData(sharedList: self.sharedUsersCellUIData, state: 1)
        for user in addedUsersList{
            user.removeUserClosure = updateUserListClosure
        }
        
        //MARK: cellUIData?[2] 新增帳務區塊最下方的【被新增者資訊】
        if let cell = cellUIData?[2] as? PayUsersTableViewCellData{
            cell.userList = addedUsersList
        }
        self.dataTableView.reloadData()
    }
    
    /**
     根據指定使用者更新
     */
    func updateSharedUserData(user: PayUsersDetailTableViewCellData){
        if let index = sharedUsersCellUIData?.firstIndex(where: { $0.userEmail == user.userEmail }),
           let oriUser = sharedUsersCellUIData?[index] {
            oriUser.payUserState = user.payUserState
            oriUser.payMoney = user.payMoney
        }
        self.updateUserUIData()
    }
    /**
     根據status分類TableView要顯示的資料
     */
    /// - Parameter sharedList: 欲整理的shared user list
    /// - Parameter state: 0 = 把payUserState=0的名單push到添加分帳者頁面(AddSharedUserViewController)   1 = 把payUserState=1的名單放到已被新增者資訊cell
    func classifyStatusesInUserUIData(sharedList: [PayUsersDetailTableViewCellData]?, state: Int) -> [PayUsersDetailTableViewCellData]{
        var addedList:[PayUsersDetailTableViewCellData] = []
        if let sharedList {
            //被新增者(payUserState = 1)的名單
            if state == 1{
                for data in sharedList{
                    if data.payUserState == 1{
                        addedList.append(data)
                    }
                }
            }
            //尚未被新增者(payUserState = 0)的名單
            else{
                for data in sharedList{
                    if data.payUserState != 1{
                        addedList.append(data)
                    }
                }
            }
        }
        return addedList
    }

    //MARK: - IBActions
    /**
     回上一頁
     */
    @IBAction func backLastPage(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     btn-完成（新增資料）
     */
    @IBAction func addNewData(_ sender: UIButton) {
        var addedList: [Payer]  = []
        if let sharedUsersCellUIData{
            for user in sharedUsersCellUIData{
                let newDbUser = Payer()
                if user.payUserState == 1{
                    let newDbUser = Payer()
                    newDbUser.payerID = user.uid
                    newDbUser.payerName = user.userName
                    newDbUser.payerEmail = user.userEmail
                    newDbUser.pay = user.payMoney
                    newDbUser.state = user.payUserState
                    newDbUser.payStatus = false
                    addedList.append(newDbUser)
                }
            }
        }
        /// - parameter uid 帳務id
        /// - parameter ledgerID 所屬帳本id
        /// - parameter creatorName 新增者
        /// - parameter billTitle 帳務標題
        /// - parameter billRemark 帳務備註
        /// - parameter payAmount 總金額
        /// - parameter payerList 付款名單
        if let db = dbParent, let ledgerId = self.ledgerId {
            if let billTitle, let billAmount{
                let newBillDetail = db.collection("BillDetails").document()
                let data = FirebaseLedgerDetails()
                data.ledgerID = ledgerId
                data.creatorName = CurrentUserDBInfo.shareInstance.myInfo?.userName
                data.creatorEmail = CurrentUserDBInfo.shareInstance.myInfo?.account
                data.billTitle = billTitle
                data.billRemark = self.billRemark
                data.payAmount = billAmount
                data.payerList = addedList
                do{
                    try newBillDetail.setData(from: data)
                }
                catch{
                    print(error.localizedDescription)
                }
            }
            else{
                let alertController = UIAlertController(title: nil, message: "標題及金額不可為空白", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default){ [weak self] _ in
                    guard let selfVC = self else { return }
                    selfVC.dismiss(animated: true)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true)
            }
            
        }
        self.reloadSharedLedgerTableViewClosure()
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     按到tableView就收鍵盤
     tableView 增加tap手勢
     UI右側選單設定：
     1. tableView 的 selection 設為 Single Selection (可被點選)
     2. table view cell 的 Selection 設為 None (不變色)
     */
    @IBAction func closeKeyboardInTableView(_ sender: Any) {
        view.endEditing(true)
    }
}

extension BillDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellUIData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let tableViewData = cellUIData?[indexPath.row] {
            switch tableViewData {
            case let cellUIData as BillInfoTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "BillInfoTableViewCell", for: indexPath) as? BillInfoTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            case let cellUIData as AddedUserTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddedUserTableViewCell", for: indexPath) as? AddedUserTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            case let cellUIData as PayUsersTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PayUsersTableViewCell", for: indexPath) as? PayUsersTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            default:
                break
            }
            
        }
        return cell
    }
    
    
}
