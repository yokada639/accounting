//
//  SharedLedgerDetailViewController.swift
//  Ledgers
//
//  Created by Murphy on 2024/5/31.
//

import UIKit
import FirebaseAuth
/**
 帳本明細頁頁面
 */
class SharedLedgerDetailViewController: UIViewController {

    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var topSegment: UISegmentedControl!
    var originDetailInfo: [FirebaseLedgerDetails]? = []
    var cellUIData: [AnyObject]? = []

    var originMyAccounts: [FirebaseLedgerDetails]? = []
    //ledgerId / ownerId：前一頁push會把值傳過來
    var ledgerId: String?
    var ledgerOwnerId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataTableView.layer.cornerRadius = 25
        self.setDelegates()
        if let selfUid = userDefaults.string(forKey: "DBKey"), selfUid == self.ledgerOwnerId{
            self.initOwnerMenu()
        }
        else{
            self.initInvitedMenu()
        }
        getOriginDetailInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Task{
            await CurrentLedgerSharedUser.shareInstance.getUsersInfoFromFirebase(ledgerId: ledgerId ?? "")
        }
    }
    func setDelegates(){
        let nib = UINib(nibName: "BillListTableViewCell", bundle: nil)
        self.dataTableView.register(nib, forCellReuseIdentifier: "BillListTableViewCell")
        let myBillNib = UINib(nibName: "BillDetailTableViewCell", bundle: nil)
        self.dataTableView.register(myBillNib, forCellReuseIdentifier: "BillDetailTableViewCell")
        self.dataTableView.delegate = self
        self.dataTableView.dataSource = self
    }
    

    
    func dataToUIData(){
        cellUIData = []
        if let originDetailInfo, originDetailInfo.count > 0{
            for oriDetail in originDetailInfo{
                if topSegment.selectedSegmentIndex == 0{
                    let nextPageClosure: (_: String) -> () = { [weak self] uid in
                        guard let strongSelf = self else { return }
                        let sb = UIStoryboard(name: "PayerInfoViewController", bundle: nil)
                        let vc = sb.instantiateViewController(withIdentifier: "PayerInfoViewController") as! PayerInfoViewController
                        vc.detailUid = uid
                        vc.navigationItem.title = oriDetail.billTitle
                        strongSelf.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    let data = BillListTableViewCellData(uid: oriDetail.uid, title: oriDetail.billTitle, creatorName: oriDetail.creatorName, creatorEmail: oriDetail.creatorEmail, amount: oriDetail.payAmount, remark: oriDetail.billRemark, gotoListPageClosure: nextPageClosure)
                    self.cellUIData?.append(data)
                    
                }
                else{
                    if let payerList = oriDetail.payerList, payerList.count > 0{
                        for payer in payerList{
                            if payer.payerID == CurrentUserDBInfo.shareInstance.myInfo?.uid, payer.payStatus == false{
                                let data = BillDetailTableViewCellData(title: oriDetail.billTitle, creator: oriDetail.creatorName, remark: oriDetail.billRemark, amount: payer.pay)
                                self.cellUIData?.append(data)
                                break
                            }
                        }
                    }
                }
            }
        }
        self.dataTableView.reloadData()
        
    }
    
    /**
     取得帳本內所有帳務內容
     */
    func getOriginDetailInfo() {
        ProgressTool.shareInstance.progressStart()
        if let db = dbParent, let ledgerID = self.ledgerId{
            let docRef = db.collection("BillDetails").whereField("ledgerID", isEqualTo: ledgerID)
            docRef.getDocuments { [weak self] snapshot, error in
                guard let snapshot, let strongSelf = self else { return }
                if !snapshot.documents.isEmpty{
                    //snapshot.documents.compactMap{ ... } 將query的資料轉進自訂的class
                    let detailList = snapshot.documents.compactMap{ [weak self] snapshot in
                        try? snapshot.data(as: FirebaseLedgerDetails.self)
                    }
                    strongSelf.originDetailInfo = detailList
                    strongSelf.dataToUIData()
                }
            }
        }
        ProgressTool.shareInstance.progressEnd()
    }
    
    /**
     初始化menu選單(帳本擁有者為自己)
     */
    func initOwnerMenu(){
        menuBtn.showsMenuAsPrimaryAction = true
        menuBtn.menu = UIMenu(children: [
                UIAction(title: "新增帳務",
                         image: UIImage(named: "add-60"),
                         handler: { [weak self] _ in
                             guard let strongSelf = self else { return }
                             strongSelf.pushToBillDetailVC()
                         }),
//                UIAction(title: "複製邀請碼",
//                         image: UIImage(named: "tools_copy-60"),
//                         handler: { [weak self] _ in
//                             guard let strongSelf = self else { return }
//                             //TODO: 複製邀請碼
//                             UIPasteboard.general.string = ""
//                         }),
                UIAction(title: "共享者管理",
                         image: UIImage(named: "tools_permissions-60"),
                         handler: { [weak self] _ in
                             guard let strongSelf = self else { return }
                             let permissionVC = UIStoryboard(name: "PermissionViewController", bundle: nil)
                                 .instantiateViewController(identifier: "PermissionViewController") as! PermissionViewController
                             permissionVC.ledgerId = strongSelf.ledgerId
                             strongSelf.navigationController?.pushViewController(permissionVC, animated: true)
                             
                         }),
                UIAction(title: "刪除帳本",
                         image: UIImage(named: "tools_delete-60"),
                         handler: { [weak self] _ in
                             guard let strongSelf = self else { return }
                             let alertController = UIAlertController(title: "是否要刪除帳本", message: "【重要】\r\n本動作無法回復，請小心操作", preferredStyle: .alert)
                             let okAction = UIAlertAction(title: "確認", style: .default) { [weak self] _ in
                                 guard let strongSelf2 = self else { return }
                                 if let db = dbParent, let ledgerID = strongSelf.ledgerId{
                                     print("database刪除動作開始執行")
//MARK: - 完成區
                                     //刪除Firebase BillDetails明細
                                     for detail in strongSelf2.originDetailInfo ?? []{
                                         db.collection("BillDetails").document(detail.uid ?? "").delete()
                                     }
//MARK: - 完成區
                                     //取得帳本內(Firebase Ledger)分享者跟擁有者資訊
                                     let usersIDList = CurrentLedgerSharedUser.shareInstance.getUsersUID()
                                     
                                     Task{
                                         for sharedUser in usersIDList{
                                             //刪除User內ownedLedgers/invitedLedgers的帳本
                                             let data = try? await db.collection("User").document(sharedUser).getDocument(as: FirebaseUserInfo.self)
                                             if let data{
                                                 //刪除自己的帳本
                                                 if let index = data.ownedLedgers?.firstIndex(of: ledgerID){
                                                     data.ownedLedgers?.remove(at: index)
                                                 }
                                                 //刪除被邀請的帳本
                                                 if let index = data.invitedLedgers?.firstIndex(of: ledgerID){
                                                     data.invitedLedgers?.remove(at: index)
                                                 }
                                                 try? db.collection("User").document(sharedUser).setData(from: data.self, merge: true)
//                                                 strongSelf.getOriginDetailInfo()
                                                 strongSelf.dataTableView.reloadData()
                                                 
                                             }
                                             let docRef = db.collection("User").whereField("unconfirmedLedgers", arrayContainsAny: [ledgerID])
                                             docRef.getDocuments { [weak self] snapshot, error in
                                                 guard let snapshot, let _ = self else { return }
                                                 if !snapshot.documents.isEmpty{
                                                     //snapshot.documents.compactMap{ ... } 將query的資料轉進自訂的class
                                                     //刪除尚未確認的帳本
                                                     let unconfirmedUsers = snapshot.documents.compactMap{ [weak self] snapshot in
                                                         try? snapshot.data(as: FirebaseUserInfo.self)
                                                     }
                                                     for user in unconfirmedUsers{
                                                         if let index = user.unconfirmedLedgers?.firstIndex(of: ledgerID){
                                                             user.unconfirmedLedgers?.remove(at: index)
                                                         }
                                                         try? db.collection("User").document(user.uid ?? "").setData(from: user.self, merge: true)
                                                     }
                                                 }
                                             }
                                             
                                         }
                                     }
                                     Task{
                                         try? await db.collection("Ledger").document(ledgerID).delete()
                                         strongSelf.navigationController?.popViewController(animated: true)
                                     }
                                     
//                                         docRef.getDocument { document, error in
                                             
//                                         }
                                         
//                                         var docRef = db.collection("User").whereField("ownedLedgers", isEqualTo: sharedUser)
//                                         docRef.getDocuments { [weak self ] snapshot, error in
//                                             guard let snapshot, let selfVC = self else { return }
//                                             if !snapshot.documents.isEmpty{
//                                                 db.collection("User").document(sharedUser).update
//                                             }
//                                         }
                                     
//                                     var docRef = db.collection("Ledger").whereField("ledgerId", isEqualTo: ledgerID)
//                                     docRef.getDocuments { [weak self ] snapshot, error in
//                                         guard let snapshot, let selfVC = self else { return }
//                                         var tempList = FirebaseLedgerInfo()
//                                         if !snapshot.documents.isEmpty{
//                                             
//                                         }
//                                     }
                                     
                                     
                                     
                                     
                                 }
                             }
                             let cancalAction = UIAlertAction(title: "取消", style: .cancel)
                             
                             alertController.addAction(okAction)
                             alertController.addAction(cancalAction)
                             strongSelf.present(alertController, animated: true)
                             
                         }),
        ])
    }
    
    /**
     初始化menu選單(帳本擁有者為別人)
     */
    func initInvitedMenu(){
        menuBtn.showsMenuAsPrimaryAction = true
        menuBtn.menu = UIMenu(children: [
                UIAction(title: "新增分帳",
                         image: UIImage(named: "add-60"),
                         handler: { [weak self] _ in
                             guard let strongSelf = self else { return }
                             strongSelf.pushToBillDetailVC()
                         }),
                UIAction(title: "退出",
                         image: UIImage(named: "tools_sign-out-60"),
                         handler: { [weak self] _ in
                             guard let strongSelf = self else { return }
                             let alertController = UIAlertController(title: nil, message: "確定要退出嗎？", preferredStyle: .alert)
                             let okAction = UIAlertAction(title: "確定", style: .default) { [weak self] _ in
                                 guard let strongSelf = self else { return }
                                 Task{
                                     await strongSelf.leaveLedger()
                                     strongSelf.navigationController?.popViewController(animated: true)
                                 }
                             }
                             let cancelAction = UIAlertAction(title: "取消", style: .cancel)
                             alertController.addAction(okAction)
                             alertController.addAction(cancelAction)
                             strongSelf.present(alertController, animated: true)
                         }),
        ])
    }
    
    func leaveLedger() async{
        if let db = dbParent, let userId = CurrentUserDBInfo.shareInstance.myInfo?.uid, let ledgerId{
            //刪除User集合內invitedLedgers的LedgerId
            var docRef = db.collection("User").document(userId)
            do{
                let document = try await docRef.getDocument()
                if document.exists{
                    if let userInfo = try? document.data(as: FirebaseUserInfo.self), let index = userInfo.invitedLedgers?.firstIndex(where: {$0 == ledgerId}){
                        userInfo.invitedLedgers?.remove(at: index)
                        try? docRef.setData(from: userInfo.self, merge: true)
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
            
            //刪除Ledger集合sharedUsers內的UserId
            docRef = db.collection("Ledger").document(ledgerId)
            do{
                let document = try await docRef.getDocument()
                if document.exists{
                    if let ledgerInfo = try? document.data(as: FirebaseLedgerInfo.self), let index = ledgerInfo.sharedUsers?.firstIndex(where: {$0 == userId}){
                        ledgerInfo.sharedUsers?.remove(at: index)
                        try? docRef.setData(from: ledgerInfo.self, merge: true)
                    }
                }
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    /**
     push到明細頁
     */
    func pushToBillDetailVC(){
        let sb = UIStoryboard(name: "BillDetailViewController", bundle: nil)
        let billDetailVC = sb.instantiateViewController(withIdentifier: "BillDetailViewController") as! BillDetailViewController
        billDetailVC.ledgerId = self.ledgerId
        billDetailVC.reloadSharedLedgerTableViewClosure = { [weak self] in
            guard let selfVC = self else { return }
            selfVC.getOriginDetailInfo()
        }
        self.navigationController?.pushViewController(billDetailVC, animated: true)
    }
    
    @IBAction func backToLedgerList(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func changeSegment(_ sender: UISegmentedControl) {
        getOriginDetailInfo()
    }
    
}

extension SharedLedgerDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellUIData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let tableViewData = cellUIData?[indexPath.row] {
            switch tableViewData {
            case let cellUIData as BillListTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "BillListTableViewCell", for: indexPath) as? BillListTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            case let cellUIData as BillDetailTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "BillDetailTableViewCell", for: indexPath) as? BillDetailTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            default:
                break
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //設定刪除Action（cell左滑）
        if topSegment.selectedSegmentIndex == 0{
            if let originDetailInfo{
                if CurrentUserDBInfo.shareInstance.myInfo?.account == originDetailInfo[indexPath.row].creatorEmail{
                    //UIContextualAction：右邊顯示出來的按鈕
                    let deleteAction = UIContextualAction(style: .destructive, title: "刪除") { [weak self] Action, view, completion in
                        guard let strongSelf = self else { return }
                        let alertController = UIAlertController(title: nil, message: "確認刪除？", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "確認", style: .default) { [weak self] _ in
                            guard let selfVC = self else { return }
                            if var originDetailInfo = selfVC.originDetailInfo, let db = dbParent, let documentUid = originDetailInfo[indexPath.row].uid{
                                db.collection("BillDetails").document(documentUid).delete()
                                selfVC.getOriginDetailInfo()
//                                Task{
//                                    await selfVC.getOriginDetailInfo()
//                                }
                                
                            }
                        }
                        let cancelAction = UIAlertAction(title: "取消", style: .cancel) { [weak self] _ in
                            guard let selfVC = self else { return }
                            selfVC.dataTableView.reloadData()
                        }
                        alertController.addAction(okAction)
                        alertController.addAction(cancelAction)
                        strongSelf.present(alertController, animated: true)
                    }
                    
                    let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
                    
                    return configuration
                }
                else{
                    let alertController = UIAlertController(title: "Oops! 刪除失敗", message: "這不是我新增的帳務", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default)
                    alertController.addAction(okAction)
                    present(alertController, animated: true)
                }
            }
        }
        return nil
    }
    
}
