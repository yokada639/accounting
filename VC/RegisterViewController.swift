//
//  RegisterViewController.swift
//  記帳
//
//  Created by Murphy on 2024/5/9.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var nicknameTextField: UITextField!
    @IBOutlet weak var nicknameBackView: UIView!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var emailBackView: UIView!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordVisibleBtn: UIButton!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordBackView: UIView!
    
    @IBOutlet weak var checkPasswordTextField: UITextField!
    @IBOutlet weak var checkPasswordVisibleBtn: UIButton!
    @IBOutlet weak var checkPasswordBackView: UIView!
    @IBOutlet weak var checkPasswordErrorLabel: UILabel!
    
    var dismissLoginVCClosure: ()->() = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegate()
        setFrame()
        self.backgroundView.layer.cornerRadius = 25
        
        emailTextField.addTarget(self, action: #selector(validEmail), for: .editingDidEnd)
        passwordTextField.addTarget(self, action: #selector(validPassword), for: .editingDidEnd)
        checkPasswordTextField.addTarget(self, action: #selector(validCheckTwicePassword), for: .editingDidEnd)
        // Do any additional setup after loading the view.
    }
    
    /**
     點空白處收鍵盤（碰到螢幕時觸發）
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK: - Objc Functions
    /**
     驗證密碼格式
     */
    @objc func validPassword(){
        if Auth.auth().currentUser == nil{
            if let password = self.passwordTextField.text{
                if !password.validatePassword(){
                    self.passwordErrorLabel.isHidden = false
                }
                else{
                    self.passwordErrorLabel.isHidden = true
                }
            }
        }
        else{
            cleanData()
        }
    }
    /**
     確認二次密碼
     */
    @objc func validCheckTwicePassword(){
        if Auth.auth().currentUser == nil{
            if passwordTextField.text != checkPasswordTextField.text{
                self.checkPasswordErrorLabel.isHidden = false
            }
            else{
                self.checkPasswordErrorLabel.isHidden = true
            }
        }
        else{
            cleanData()
        }
        
    }
    
    /**
     正則驗證email
     */
    @objc func validEmail(){
        if Auth.auth().currentUser == nil{
            if let email = self.emailTextField.text{
                if !email.validateEmail(){
                    self.emailErrorLabel.isHidden = false
                }
                else{
                    self.emailErrorLabel.isHidden = true
                }
            }
        }
        else{
            cleanData()
        }
    }
    //MARK: - Functions
    /**
     設定delegate
     */
    func setDelegate(){
        emailTextField.delegate = self
        passwordTextField.delegate = self
        checkPasswordTextField.delegate = self
    }
    
    /**
     設定frame
     */
    func setFrame(){
        let height = self.nicknameBackView.frame.height
        self.nicknameBackView.layer.cornerRadius = height/2
        self.emailBackView.layer.cornerRadius = height/2
        self.passwordBackView.layer.cornerRadius = height/2
        self.checkPasswordBackView.layer.cornerRadius = height/2
    }
    
    /**
     清除輸入內容
     */
    func cleanData(){
        self.nicknameTextField.text = ""
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        self.checkPasswordTextField.text = ""
    }
    
    /**
     新增使用者到Firebase
     */
    func addNewUserToDB(userEmail: String, userName:String, uid: String){
        if FirebaseDocSettings.shareInstance.getDbParent(){
            if let db = dbParent{
                db.collection("User")
                    .document(uid)
                    .setData([
                        "account" : userEmail,
                        "userName" : userName,
                        "ownedLedgers" : [String](),
                        "invitedLedgers" : [String](),
                        "unconfirmedLedgers" : [String](),
                        "enable" : true
                    ], merge: true)
            }
        }
        
    }
    
//MARK: - IBActions
    /**
     btn - 重新填寫(清除輸入)
     */
    @IBAction func refill(_ sender: UIButton) {
        cleanData()
    }
    
    /**
     btn - 送出(註冊)
     */
    @IBAction func register(_ sender: UIButton) {
        if let userName = self.nicknameTextField.text, (userName.count > 0 && userName.count <= 8),
           let emailAddress = self.emailTextField.text, emailAddress.validateEmail(),
           let password = self.passwordTextField.text, password.validatePassword(),
           let checkPassword = self.checkPasswordTextField.text, password == checkPassword{
            //建立使用者
            Auth.auth().createUser(withEmail: emailAddress, password: password) { [weak self] result, error in
                guard let selfVC = self else { return }
                guard let user = result?.user, error == nil else {
                    let alertController = UIAlertController(title: "Oops! 註冊失敗", message: "E-mail已存在，請重新輸入", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
                        guard let selfVC = self else { return }
                        selfVC.cleanData()
                    }
                    alertController.addAction(okAction)
                    selfVC.present(alertController, animated: true)
                    return
                }
                //建立使用者到Firebase
                if let email = user.email{
                    selfVC.addNewUserToDB(userEmail: email, userName: userName, uid: user.uid)
                }
                
                
                //回登入頁面
                let alertController = UIAlertController(title: nil, message: "註冊成功", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
                    self?.dismiss(animated: false)
                    self?.dismissLoginVCClosure()
                }
                alertController.addAction(okAction)
                
                selfVC.present(alertController, animated: true)
                
            }
        }
        else{
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            
            if let nickname = self.nicknameTextField.text, (nickname.count == 0 || nickname.count > 8){
                if nickname.count == 0{
                    alertController.message = "請輸入暱稱"
                }
                else if nickname.count > 8{
                    alertController.message = "暱稱不得超過8個字"
                }
            }
            else if let emailAddress = self.emailTextField.text, !emailAddress.validateEmail(){
                alertController.message = "電子郵件信箱格式有誤"
            }
            else if let password = self.passwordTextField.text, !password.validatePassword(){
                alertController.message = "密碼需八碼以上並開頭須為字母，其中至少包含一個數字"
            }
            else if let password = self.passwordTextField.text,
                    let checkPassword = self.checkPasswordTextField.text,
                    password != checkPassword{
                alertController.message = "確認密碼內容不符"
            }
            else if let emailAddress = self.emailTextField.text, emailAddress.count == 0{
                alertController.message = "請輸入帳號"
            }
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true)
            
        }
        

    }
    
    /**
     btn - 密碼可見／不可見
     */
    @IBAction func passwordIsVisible(_ sender: UIButton) {
        if passwordTextField.isSecureTextEntry{
            passwordTextField.isSecureTextEntry = false
            passwordVisibleBtn.setImage(UIImage(named: "visible-48"), for: .normal)
        }
        else{
            passwordTextField.isSecureTextEntry = true
            passwordVisibleBtn.setImage(UIImage(named: "invisible-48"), for: .normal)
        }
    }
    
    /**
     btn - 確認密碼可見／不可見
     */
    @IBAction func checkPasswordIsVisible(_ sender: UIButton) {
        if checkPasswordTextField.isSecureTextEntry{
            checkPasswordTextField.isSecureTextEntry = false
            checkPasswordVisibleBtn.setImage(UIImage(named: "visible-48"), for: .normal)
        }
        else{
            checkPasswordTextField.isSecureTextEntry = true
            checkPasswordVisibleBtn.setImage(UIImage(named: "invisible-48"), for: .normal)
        }
    }


}

//MARK: - extension UITextFieldDelegate
extension RegisterViewController: UITextFieldDelegate{
    /**
     (delegate)按下return鍵：用return鍵收鍵盤
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /**
     *(delegate)開始進行編輯－將textfield內容清空，錯誤訊息隱藏
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.passwordTextField{
            passwordErrorLabel.isHidden = true
            passwordTextField.text = ""
        }
        else if textField == self.checkPasswordTextField{
            checkPasswordErrorLabel.isHidden = true
            checkPasswordTextField.text = ""
        }
            
    }
}
