//
//  AddNewLedgerViewController.swift
//  記帳
//
//  Created by Murphy on 2024/5/12.
//

import UIKit
import FirebaseFirestore

class AddNewLedgerViewController: UIViewController {
    @IBOutlet weak var ledgerNameTextField: UITextField!
    @IBOutlet weak var addLedgerView: UIView!
    @IBOutlet weak var shareUserTableView: UITableView!

    var shareUserList = [FirebaseUserInfo]()
    var shareUserListData: [AnyObject]? = []

    var reloadCollectionViewClosure: ()->() = {}
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenHeight = UIScreen.main.bounds.height
        setFrame()
        setDelegates()
    }
    
    /**
     點螢幕收小鍵盤
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
//MARK: - IBActions
    /**
     新增帳本
     */
    @IBAction func addLedger(_ sender: UIButton) {
        if let ledgerName = ledgerNameTextField.text, 
            (ledgerName.count > 6 || ledgerName.count == 0){
            var msgText = ""
            if ledgerName.count > 6{
                msgText = "帳本名稱超過六個字"
            }
            else{
                msgText = "請輸入帳本名稱"
            }
            let alertController = UIAlertController(title: nil, message: msgText, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(okAction)
            present(alertController, animated: true)
        }
        else{
            let myDBInfo = CurrentUserDBInfo.shareInstance.myInfo
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            if let ledgerName = ledgerNameTextField.text, let myUid = myDBInfo?.uid, let name = myDBInfo?.userName, let db = dbParent, let account = myDBInfo?.account  {
                //MARK: 新增帳本
                /// - Parameter ledgerId: Firebase 帳本 ID, 等同document ID
                /// - Parameter ownerUid 擁有者uid
                /// - Parameter ownerName 擁有者中文名稱
                /// - Parameter ledgerName 帳本中文名稱
                /// - Parameter ownerEmail 擁有者Email
                /// - Parameter sharedUsers 共享者名單(自己是第一個)
                let newLedger = db.collection("Ledger").document()
                newLedger.setData(["ledgerId" : newLedger.documentID,
                                   "ownerUid" : myUid,
                                   "ownerName" : name,
                                   "ledgerName" : ledgerName,
                                   "ownerEmail" : account,
                                   "sharedUsers" : [myUid]
                                   ])
                
                //MARK: 添加新帳本Id到使用者帳本清單
                myDBInfo?.ownedLedgers?.append(newLedger.documentID)
                try? db.collection("User").document(myUid).setData(from: myDBInfo.self, merge: true)
                
                //MARK: 把帳本id添加到shareUserList的unconfirmedLedgers
                if self.shareUserList.count > 0{
                    for user in shareUserList{
                        user.unconfirmedLedgers?.append(newLedger.documentID)
                        if let userUid = user.uid{
                            try? db.collection("User").document(userUid).setData(from: user.self, merge: true)
                        }
                    }
                }
                alertController.message = "帳本新增成功"
            }
            else{
                alertController.message = "帳本新增失敗"
            }
            let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] action in
                self?.dismiss(animated: true)
                self?.reloadCollectionViewClosure()
            }
            alertController.addAction(okAction)
            present(alertController, animated: true)
        }
    }
    /**
     回帳本列表
     */
    @IBAction func backToLedgerList(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    /**
     邀請共享對象
     */
    @IBAction func inviteUser(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: "請輸入邀請對象E-mail", preferredStyle: .alert)
        //alert添加Textfield
        alertController.addTextField()
        let okAction = UIAlertAction(title: "確定", style: .default) { [weak self] _ in
            //用index取alertController上第一個textField: alertController.textFields?[0]
            guard let input = alertController.textFields?[0] else{ return }
            
            //讓Firebase去Query使用者
            self?.queryInviteUser(email: input.text ?? "")
        }
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true)
    }
    
//MARK: - Functions
    /**
     設定Frame
     */
    func setFrame(){
        self.addLedgerView.layer.cornerRadius = 15
    }
    
    /**
     設定Delegate
     */
    func setDelegates(){
        let nib = UINib(nibName: "AddNewLedgerTableViewCell", bundle: nil)
        self.shareUserTableView.register(nib, forCellReuseIdentifier: "AddNewLedgerTableViewCell")
        self.shareUserTableView.delegate = self
        self.shareUserTableView.dataSource = self
        self.ledgerNameTextField.delegate = self
        
    }
    
    /**
     轉成tableView  UI Data
     */
    func setCellUIData(){
            shareUserListData = []
            for shareUser in shareUserList {
                let cellData = AddNewLedgerTableViewCellData.init(email: shareUser.account, name: shareUser.userName)
                shareUserListData?.append(cellData)
            }
            self.shareUserTableView.reloadData()
    }
    /**
     email查詢firebase內邀請的使用者資訊
     query回傳有可能為多個結果(此處為單一結果），要用getDocuments去接
     */
    func queryInviteUser(email: String) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        if let db = dbParent{
            let docRef = db.collection("User").whereField("account", isEqualTo: email).whereField("enable", isEqualTo: true)
            docRef.getDocuments { [weak self] snapshot, error in
                guard let snapshot, let strongSelf = self else { return }
                if snapshot.documents.isEmpty{
                    alertController.message = "查無使用者"
                }
                else{
                    //snapshot.documents.compactMap{ ... } 將query的資料轉進自訂的class
                    let userList = snapshot.documents.compactMap{ [weak self] snapshot in
                        try? snapshot.data(as: FirebaseUserInfo.self)
                    }
                    for user in userList{
                        if let selfUid = userDefaults.string(forKey: "DBKey"), selfUid != user.uid{
                            if let index = strongSelf.shareUserList.firstIndex(where: {$0.uid == user.uid}){
                                alertController.message = "使用者已被添加"
                            }
                            else{
                                strongSelf.shareUserList.append(user)
                                alertController.message = "加入成功"
                            }
                        }
                        else{
                            alertController.message = "邀請對象不可為自己"
                        }
                    }
                }
                let okAction = UIAlertAction(title: "確定", style: .default)
                alertController.addAction(okAction)
                strongSelf.present(alertController, animated: true)
                strongSelf.shareUserTableView.reloadData()
                strongSelf.setCellUIData()
            }
        }
    }
}

//MARK: - extension UITableViewDelegate, UITableViewDataSource
extension AddNewLedgerViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shareUserListData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let tableViewData = shareUserListData?[indexPath.row] {
            switch tableViewData {
            case let cellUIData as AddNewLedgerTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddNewLedgerTableViewCell", for: indexPath) as? AddNewLedgerTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            default:
                break
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //UIContextualAction：右邊顯示出來的按鈕
        let deleteBtn = UIContextualAction(style: .destructive, title: "刪除") { [weak self] Action, view, completion in
            guard let strongSelf = self else { return }
            let alertController = UIAlertController(title: nil, message: "確認刪除？", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "確認", style: .default) { [weak self] _ in
                guard let selfVC = self else { return }
                selfVC.shareUserList.remove(at: indexPath.row)
                strongSelf.setCellUIData()
                
            }
            let cancelAction = UIAlertAction(title: "取消", style: .cancel) { [weak self] _ in
                guard let selfVC = self else { return }
                strongSelf.shareUserTableView.reloadData()
                
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            strongSelf.present(alertController, animated: true)
        }
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteBtn])
        
        return configuration

    }
    
    
}

//MARK: - extension UITextFieldDelegate
extension AddNewLedgerViewController: UITextFieldDelegate{
    /**
     用return鍵收鍵盤
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // 收起鍵盤
        return true
    }
}
