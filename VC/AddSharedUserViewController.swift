//
//  AddSharedUserViewController.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/23.
//

import UIKit
/**
 輸入分帳金額頁面
 */
class AddSharedUserViewController: UIViewController {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var filterBG: UIView!
    @IBOutlet weak var filterTextField: UITextField!
    @IBOutlet weak var usersTableView: UITableView!
    
    var userList: [PayUsersDetailTableViewCellData]?
    var oriCellUIData: [AnyObject]? = [] //userList轉為Cell UI Data後的原始資料
    var cellUIData: [AnyObject]? = [] //真正顯示在畫面上的Cell UI Data
    var updateUserInfoClosure: ( _ : PayUsersDetailTableViewCellData ) -> () = { _ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setFrames()
        self.setDelegates()
        self.setCellUIData()
        
        // Do any additional setup after loading the view.
    }
    
    /**
     點螢幕收textfield鍵盤
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.filterTextField.resignFirstResponder()
    }
    
    func setFrames(){
        self.backView.layer.cornerRadius = 25
        self.filterBG.layer.cornerRadius = 18
    }

    func setDelegates(){
        let nib = UINib(nibName: "PayUsersDetailTableViewCell", bundle: nil)
        self.usersTableView.register(nib, forCellReuseIdentifier: "PayUsersDetailTableViewCell")
        self.usersTableView.delegate = self
        self.usersTableView.dataSource = self
        
        self.filterTextField.delegate = self
    }
    
    func setCellUIData(){
        self.cellUIData = []
        //updatePayUser: cell內按下新增按鈕後的closure
        let updatePayUser: (_: PayUsersDetailTableViewCellData) -> () = { [weak self] user in
            guard let strongSelf = self else { return }
            //跳出alert輸入金額
            let alertController = UIAlertController(title: nil, message: "請輸入金額", preferredStyle: .alert)
            //alert添加Textfield
            alertController.addTextField { [weak self] textField in
                guard let strongSelf = self else { return }
                textField.keyboardType = .numberPad
            }
            let okAction = UIAlertAction(title: "確定", style: .default) { [weak self] _ in
                guard let strongSelf2 = self, let amount = alertController.textFields?[0].text else { return }
                //用index取alertController上第一個textField: alertController.textFields?[0]
                if amount.validateAmount(){
                    user.payMoney = amount
                    user.payUserState = 1

                    strongSelf2.updateCellUIData(user: user)
                    strongSelf2.updateUserInfoClosure(user)
                }
                else{
                    let errorAlertController = UIAlertController(title: nil, message: "不可輸入0及非數字字元", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "確認", style: .default)
                    errorAlertController.addAction(okAction)
                    strongSelf2.present(errorAlertController, animated: true)
                }
                
                
            }
            
            alertController.addAction(okAction)
            
            let cancelAction = UIAlertAction(title: "取消", style: .cancel) 
            alertController.addAction(cancelAction)
            strongSelf.present(alertController, animated: true)
        }
        
        //將傳進來的userList所有user添加update closure方法
        if let userList, userList.count > 0{
            for user in userList{
                user.addPayUserClosure = updatePayUser
                self.cellUIData?.append(user)
            }
            self.oriCellUIData = cellUIData
            self.usersTableView.reloadData()
            
        }
    }
 
    /**
     更新畫面(資料用updateUserInfoClosure去BillDetailVC更新）
     */
    func updateCellUIData(user: PayUsersDetailTableViewCellData){
        if let userIndex = (cellUIData as? [PayUsersDetailTableViewCellData])?.firstIndex(where: { $0.userEmail == user.userEmail }){
            self.cellUIData?.remove(at: userIndex)
        }
        self.usersTableView.reloadData()
    }
    
    /**
     篩選使用者
     */
    /// - Parameter text 修改後的文字
    func filterUser(text: String){
        var filterUIData:[AnyObject] = []
        if text == ""{
            for data in oriCellUIData ?? []{
                if let data = data as? PayUsersDetailTableViewCellData{
                    if data.payUserState == 0 {
                        filterUIData.append(data)
                    }
                }
            }
        }
        else{
            if let oriCellUIData{
                for data in oriCellUIData{
                    switch(data){
                    case let data as PayUsersDetailTableViewCellData:
                        if let name = data.userName{
                            if name.contains(text){
                                filterUIData.append(data)
                            }
                        }
                    default:
                        break
                    }
                }
            }
            
        }
        self.cellUIData = filterUIData
        self.usersTableView.reloadData()
    }
    
    @IBAction func backLastPage(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeKeyboard(_ sender: Any) {
        view.endEditing(true)
    }
}

extension AddSharedUserViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellUIData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let tableViewData = cellUIData?[indexPath.row] {
            switch tableViewData {
            case let cellUIData as PayUsersDetailTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PayUsersDetailTableViewCell", for: indexPath) as? PayUsersDetailTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            default:
                break
            }
        }
        return cell
    }
    
    
}

extension AddSharedUserViewController: UITextFieldDelegate{
    /**
     用return鍵收鍵盤
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // 收起鍵盤
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let originText = textField.text,
           let textRange = Range(range, in: originText) {
            let updatedText = originText.replacingCharacters(in: textRange, with: string)
            self.filterUser(text: updatedText)
        }
        return true
    }
}
