//
//  MainViewController.swift
//  記帳
//
//  Created by Murphy on 2024/4/5.
//

import UIKit
import FirebaseAuth

//進入點的TabbarController
class TabBarInitViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //抓StoryBoard
        let ledgerSB = UIStoryboard(name: "DetailViewController", bundle: nil)
        let ledgerListSB = UIStoryboard(name: "LedgerListViewController", bundle: nil)
        
        //抓ViewController
        if let myLedgerVC = ledgerSB.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController,
            let ledgerListVC = ledgerListSB.instantiateViewController(withIdentifier: "LedgerListViewController") as? LedgerListViewController{
            //設定Navigation
            let myLedgerNAV = UINavigationController.init(rootViewController: myLedgerVC)
            let ledgerListNAV = UINavigationController.init(rootViewController: ledgerListVC)
            
            
            //設定下方tabbar item的圖案跟文字
            myLedgerNAV.tabBarItem = UITabBarItem(title: "我的帳本", image: UIImage(named: "ledger-48"), selectedImage: UIImage(named: "ledger-48"))
            ledgerListNAV.tabBarItem = UITabBarItem(title: "雲端共享帳本", image: UIImage(named: "cloud-development-48"), selectedImage: UIImage(named: "cloud-development-48"))
                
            //加入tab bar Controller
            self.addChild(myLedgerNAV)
            self.addChild(ledgerListNAV)
            
            if #available(iOS 15.0, *){
                initNavigationBarAppearance(nav: myLedgerNAV, customColor: UIColor(red: 160.0/255, green: 196.0/255, blue: 226.0/255, alpha: 1.0))
                initNavigationBarAppearance(nav: ledgerListNAV, customColor: UIColor(red: 255.0/255, green: 217.0/255, blue: 81.0/255, alpha: 1.0))
                initTabBarAppearance()
            }
        }
    }
    /**
     tab bar controller背景色取消淡入淡出
     */
    func initTabBarAppearance(){
        let appearance = UITabBarAppearance.init()
        
//        appearance.configureWithOpaqueBackground()
        appearance.shadowColor = .white
        appearance.backgroundColor = .white
        self.tabBar.standardAppearance = appearance
        self.tabBar.scrollEdgeAppearance = appearance
    }

    /**
     navigation controller背景色取消淡入淡出
     */
    func initNavigationBarAppearance(nav: UINavigationController, customColor: UIColor){
        let apperance = UINavigationBarAppearance.init()
        
        apperance.shadowColor = customColor
        apperance.backgroundColor = customColor
        
        nav.navigationBar.standardAppearance = apperance
        nav.navigationBar.scrollEdgeAppearance = apperance
    }
    
    
}

extension TabBarInitViewController: UITabBarControllerDelegate{
    /**
     tabbar被點選時
     */
    /// - parameter tabBar 下面的tabBar
    /// - parameter item 被點選的tab
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        //MARK: 取得被點選的tab index: tabBar.items?.firstIndex(of: item)
        if let indexOfTab = tabBar.items?.firstIndex(of: item), indexOfTab == 1{
            //確認是否有currentUser
            if Auth.auth().currentUser == nil {
                if let loginVC = UIStoryboard(name: "LoginViewController", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController{
                    //MARK: closure-返回我的帳本頁面
                    loginVC.backToMyLedgerClosure = { [weak self] in
                        self?.selectedIndex = 0
                    }
                    
                    
                    /* 棄用（重複呼叫Api），改用LedgerListVC的viewWillAppear去抓Firebase資料
                    // MARK: closure-登入成功後去重新取firebase資料 */
//                    loginVC.goToLedgerListClosure = { [weak self] in
//                        MARK: 取得tabBarController目前選取的view controller： self?.selectedViewController(會取到nav層) + nav.viewControllers.first取第一個VC(LedgerListViewController)
//                        if let ledgerVCNav = self?.selectedViewController as? UINavigationController, let ledgerVC = ledgerVCNav.viewControllers.first as? LedgerListViewController{
//                            FirebaseDocSettings.shareInstance.getcurrentUserAndLedgers(vc: ledgerVC)
//                        }
//                        else{
//                            print("抓不到LedgerVC")
//                        }
//                        Task{
//                            await CurrentUserDBInfo.shareInstance.getInfo()
//                        }
//                    }
                    
                    
                    loginVC.modalPresentationStyle = .fullScreen
                    self.present(loginVC, animated: true)
                    
                }
            }
            
            
        }
        
    }
}
