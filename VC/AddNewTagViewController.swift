//
//  AddNewTagViewController.swift
//  記帳
//
//  Created by Murphy on 2024/5/5.
//

import UIKit

class AddNewTagViewController: UIViewController {

    @IBOutlet weak var tagImage: UIImageView!
    
    @IBOutlet weak var editTextField: UITextField!
    
    @IBOutlet weak var editBackView: UIView!
    
    @IBOutlet weak var iconsBackView: UIView!
    
    @IBOutlet weak var iconCollectionView: UICollectionView!
    
    var customTagImagesList: [String]?
    
    var selectedCellIndexPath: IndexPath?
    var currentTags:[ItemTagModel]?
    
    var reloadDataTableviewClosure: ()->() = {}
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "新增標籤"
        getCustomTagImagesList()
        setDelegateSettings()
        setFrames()
        
//        //監聽editTextField,文字最多４個字(會造成輸入時注音超過四個字會被切斷)
//        editTextField.addTarget(self, action: #selector(checkMaxTextLength), for: .editingChanged)
        
        // Do any additional setup after loading the view.
    }
//    @objc func checkMaxTextLength(){
//        if let textFieldText = editTextField.text{
//            if(textFieldText.count > 4){
//                editTextField.text = String(textFieldText.prefix(4))
//            }
//        }
//    }
    
//  MARK: autolayout完成後才呼叫viewDidLayoutSubviews()
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureCellSize()
    }
    
    /**
     點螢幕收textfield鍵盤
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.editTextField.resignFirstResponder()
    }
    
//  MARK: - Functions
    /**
     取得collection view 要顯示的圖片名稱列表
     */
    func getCustomTagImagesList(){
        self.customTagImagesList = userDefaults.array(forKey: "customTagImages") as? [String]
    }

    /**
     調整collection view的cell大小
     */
//MARK: 要調整cell的間距，一定要同時調整CollectionView UI內的Collection View Flow Layout
//MARK: 參數：min spacing: cell之間的間距 /section inset: 與collection view 上下左右的間距
    func configureCellSize() {
        //section左右10, min spacing 10*4 = 60 共5格
        let width = (iconCollectionView.frame.width - 70.1) / 6
       
        let flowLayout = iconCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        flowLayout?.itemSize = CGSize(width: width, height: width)
        flowLayout?.estimatedItemSize = .zero
        
        self.iconCollectionView.reloadData()
//        flowLayout?.minimumInteritemSpacing = 1
    }
    
    /**
     畫面內Frame設定
     */
    func setFrames(){
        self.iconsBackView.layer.cornerRadius = 15
        self.editBackView.layer.cornerRadius = 15
    }
    
    /**
     畫面delegate設定
     */
    func setDelegateSettings(){
        //collection view
        let nib = UINib(nibName: "CustomIconCollectionViewCell", bundle: nil)
        self.iconCollectionView.register(nib, forCellWithReuseIdentifier: "CustomIconCollectionViewCell")
        self.iconCollectionView.delegate = self
        self.iconCollectionView.dataSource = self
        
        //text field
        self.editTextField.delegate = self
    }
    
    
//  MARK: - IBActions
    /**
     btn-完成：新增自訂tag
     */
    @IBAction func addNewData(_ sender: UIButton) {
        if let cellIndex = selectedCellIndexPath, self.editTextField.text?.count ?? 0 > 0, self.editTextField.text?.count ?? 0 < 5{
            let newTag = ItemTagModel()
            newTag.tagText = self.editTextField.text
            newTag.tagImgName = self.customTagImagesList?[cellIndex.row]
            self.currentTags?.append(newTag)

            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(currentTags) {
                userDefaults.set(encoded, forKey: "itemTagList")
            }
            reloadDataTableviewClosure()
            self.dismiss(animated: true)
        }
        else{
            let textCount = self.editTextField.text?.count
            var msg = ""
            if(textCount == 0){
                msg = "請選擇圖片及輸入標籤名稱"
            }
            else if ((textCount ?? 0) > 4){
                msg = "標籤字數大於4個字"
            }
            let alertController = UIAlertController(title: "Oops! 新增失敗", message: msg, preferredStyle: .alert)
               let okAction = UIAlertAction(title: "OK", style: .default)
               alertController.addAction(okAction)
               present(alertController, animated: true)
        }
    }
    
}



//  MARK: - extension UICollectionViewDelegate, UICollectionViewDataSource
extension AddNewTagViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    /**
     return cell數量
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.customTagImagesList?.count ?? 0
    }
    
    /**
     設定cell內容
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomIconCollectionViewCell", for: indexPath) as! CustomIconCollectionViewCell
        
        if let list = self.customTagImagesList{
            cell.setCellImage(imageName: list[indexPath.row])
        }
        
        return cell
    }
    
    /**
     設定cell被點擊
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let list = customTagImagesList{
            let imageName = list[indexPath.row]
            self.tagImage.image = UIImage(named: imageName)
        }
        
        self.selectedCellIndexPath = indexPath
        
    }
}

//MARK: - extension UITextFieldDelegate
extension AddNewTagViewController: UITextFieldDelegate{
    /**
     用return鍵收鍵盤
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // 收起鍵盤
        return true
    }
}

