//
//  UncomfirmedViewController.swift
//  記帳
//
//  Created by Murphy on 2024/5/20.
//

import UIKit
import FirebaseFirestore
class UnconfirmedViewController: UIViewController {
    @IBOutlet weak var unconfirmedTableView: UITableView!
    
    var unconfirmLedgers: [FirebaseLedgerInfo]?
    var unconfirmLedgersData: [AnyObject]? = [] //cell內UI要用的data
//MARK: 棄用（重複呼叫Api） var reloadLastPageLedgers: ()->() = {}
    var acceptClosure: ( _ : String? )->() = { _ in }
    var rejectClosure: ( _ : String? )->() = { _ in }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.unconfirmedTableView.layer.cornerRadius = 25
        self.navigationItem.title = "待核准的帳本"
        setDelegates()
        
        Task{
            ProgressTool.shareInstance.progressStart()
            await CurrentUserDBInfo.shareInstance.getInfo(vc: self)
            await getUnconfirmedLedgers()
            setCellData()
            DispatchQueue.main.async {
                ProgressTool.shareInstance.progressEnd()
                self.unconfirmedTableView.reloadData()
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
//        reloadLastPageLedgers()
        self.navigationController?.popViewController(animated: true)
    }
    /**
     設定delegate
     */
    func setDelegates(){
        let nib = UINib(nibName: "UnconfirmedTableViewCell", bundle: nil)
        self.unconfirmedTableView.register(nib, forCellReuseIdentifier: "UnconfirmedTableViewCell")
        self.unconfirmedTableView.delegate = self
        self.unconfirmedTableView.dataSource = self
        
    }

    /**
     取得待核准的帳本
     */
    func getUnconfirmedLedgers() async{
        self.unconfirmLedgers = [FirebaseLedgerInfo]()
        if let db = dbParent, let ledgers = CurrentUserDBInfo.shareInstance.myInfo?.unconfirmedLedgers{
            for ledgerID in ledgers{
                //抓帳本(DB該帳本全部資訊)tableName->uid->getDocument(as:自訂codable的class entity)
                let docRef = db.collection("Ledger").document(ledgerID)
                do{
                    let document = try await docRef.getDocument()
                    if document.exists{
                        if let ledgerInfo = try? document.data(as: FirebaseLedgerInfo.self){
                            self.unconfirmLedgers?.append(ledgerInfo)
                        }
                    }
                }
                catch{
                    print("Error getting document: \(error.localizedDescription)")
                }
            }
        }
    }
    
    /**
     取完資料做tableView cell UI資料
     */
    func setCellData(){
        if self.unconfirmLedgers?.count ?? 0 > 0 {
            for ledger in self.unconfirmLedgers ?? []{
                self.acceptClosure = { [weak self] ledgerId in
                    guard let selfVC = self else { return }
                    if let ledgerId,
                       let myInfo = CurrentUserDBInfo.shareInstance.myInfo,
                       let myUid = myInfo.uid,
                       let db = dbParent{
                        myInfo.invitedLedgers?.append(ledgerId)
                        //取陣列index：array.firstIndex(of: 該資料的值)
                        if let index = myInfo.unconfirmedLedgers?.firstIndex(of: ledgerId){
                            myInfo.unconfirmedLedgers?.remove(at: index)
                        }
                        //更新user unconfirmedLedgers清單
                        try? db.collection("User").document(myUid).setData(from: myInfo.self, merge: true)
                        ledger.sharedUsers?.append(myUid)
                        
                        //MARK: 更新sharedUsers清單(方法1)
                        try? db.collection("Ledger").document(ledgerId).setData(from: ledger.self, merge: true)
                        //MARK: 更新sharedUsers清單(方法2)
//                        let updateData: [String: Any] = [ "sharedUsers" : ledger.sharedUsers ?? []]
//                        db.collection("Ledger").document(ledgerId).updateData(updateData)
                        
                    }
                    selfVC.reloadUnconfirmedData(acceptClosure: selfVC.acceptClosure, rejectClosure: selfVC.rejectClosure)
//MARK: 棄用（重複呼叫Api） self?.reloadLastPageLedgers()
                }
                
                self.rejectClosure = { [weak self] ledgerId in
                    guard let selfVC = self else { return }
                    if let ledgerId,
                       let db = dbParent,
                       let myInfo = CurrentUserDBInfo.shareInstance.myInfo,
                       let myUid = myInfo.uid{
                        if let index = myInfo.unconfirmedLedgers?.firstIndex(of: ledgerId){
                            myInfo.unconfirmedLedgers?.remove(at: index)
                        }
                        try? db.collection("User").document(myUid).setData(from: myInfo.self, merge: true)
                    }
                    selfVC.reloadUnconfirmedData(acceptClosure: selfVC.acceptClosure, rejectClosure: selfVC.rejectClosure)
//MARK: 棄用（重複呼叫Api） self?.reloadLastPageLedgers()
                }
                let cellData = UnconfirmedTableViewCellData.init(ownerName: ledger.ownerName, ledgerNameString: ledger.ledgerName, ledgerId: ledger.ledgerId, acceptClosure: acceptClosure, rejectClosure: rejectClosure)
                unconfirmLedgersData?.append(cellData)
            }
            
            
        }
    }
    
    /**
     重新取得尚未確認的帳本名單
     */
    func reloadUnconfirmedData(acceptClosure: @escaping ( _ : String? )->(), rejectClosure: @escaping ( _ : String? )->()){
        //先將dataList清除掉
        self.unconfirmLedgersData?.removeAll()
        Task{
            await self.getUnconfirmedLedgers()
            if self.unconfirmLedgers?.count ?? 0 > 0{
                for ledger in unconfirmLedgers ?? []{
                    let cellData = UnconfirmedTableViewCellData.init(ownerName: ledger.ownerName, ledgerNameString: ledger.ledgerName, ledgerId: ledger.ledgerId, acceptClosure: acceptClosure, rejectClosure: rejectClosure)
                    unconfirmLedgersData?.append(cellData)
                }
            }
            self.unconfirmedTableView.reloadData()
        }
    }
    
}


//MARK: - extension UITableViewDelegate, UITableViewDataSource
extension UnconfirmedViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return unconfirmLedgersData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let tableViewData = unconfirmLedgersData?[indexPath.row] {
            switch tableViewData {
            case let cellUIData as UnconfirmedTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "UnconfirmedTableViewCell", for: indexPath) as? UnconfirmedTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            default:
                break
            }
            
        }
        return cell
        
//改用MVVM 架構
//        if let dbInfo = myDBInfo,
//            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "UnconfirmedTableViewCell", for: indexPath) as? UnconfirmedTableViewCell,
//            let ledger = self.unconfirmLedgers?[indexPath.row]{
//            
//            
//            tableViewCell.setCell(user: dbInfo, ledger: ledger)
//            tableViewCell.reloadTableViewClosure = { [weak self] in
//                guard let selfVC = self else { return }
//                Task{
//                    await self?.getUnconfirmedLedgers()
//                    self?.unconfirmedTableView.reloadData()
//                }
//            }
//            cell = tableViewCell
//        }
        
    }
    
    
}
