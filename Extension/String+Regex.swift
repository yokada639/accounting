//
//  String+Regex.swift
//  記帳
//
//  Created by Murphy on 2024/5/10.
//

import Foundation

extension String{
    /**
     正則驗證密碼(用字串直接呼叫方法)，符合即回傳true
     */
    /// - Parameter 條件：(?=.*[0-9]) 整個字串至少有一個數字
    /// - Parameter 條件：[A-Za-z][a-zA-Z0-9]{7,} 開頭英文字母，其餘為英文大小寫或數字
    func validatePassword() -> Bool{
        let pattern = "^(?=.*[0-9])[A-Za-z][a-zA-Z0-9]{7,}$"
        
        guard let regex = try? NSRegularExpression(pattern: pattern, options: []) else{return false}
        let matchs = regex.matches(in: self, range: NSRange(location: 0, length: self.count))
        
        return matchs.count > 0
    }
    
    /**
     正則驗證Email(用字串直接呼叫方法)，符合即回傳true
     */
    /// - Parameter 條件：[A-Z0-9a-z] 開頭為大小寫英文字母或數字
    /// - Parameter 條件：[A-Z0-9a-z._%+-]+ 接受1個以上的大小寫英文字母、數字、._%+-符號
    /// - Parameter 條件：@ 接受後續為1個@
    /// - Parameter 條件：[A-Za-z0-9.-]+ 接受1個以上的大小寫英文字母、數字
    /// - Parameter 條件：\\. 接受一個.
    /// - Parameter 條件：[A-Za-z]{2,4} 接受2-4個大小寫英文字母
    func validateEmail() -> Bool{
        let pattern = "^[A-Z0-9a-z][A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$"
        
        guard let regex = try? NSRegularExpression(pattern: pattern, options: []) else{return false}
        let matchs = regex.matches(in: self, range: NSRange(location: 0, length: self.count))

        return matchs.count > 0
    }
    
    /**
     正則驗證金額(用字串直接呼叫方法)，符合即回傳true
     */
    /// - Parameter 條件：[1-9] 開頭為1-9, 至少要一個
    /// - Parameter 條件：[0-9]{0,} 接下來為0-9的數字，可以有0個以上或0個
    func validateAmount() -> Bool{
        let pattern = "^[1-9][0-9]{0,}$"
        
        guard let regex = try? NSRegularExpression(pattern: pattern, options: []) else{return false}
        let matchs = regex.matches(in: self, range: NSRange(location: 0, length: self.count))

        return matchs.count > 0
    }
    
}
