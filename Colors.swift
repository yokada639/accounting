//
//  Colors.swift
//  Ledgers
//
//  Created by Murphy on 2024/7/7.
//

import Foundation
import UIKit

var yellowBG = UIColor(red: 255/255.0, green: 217/255.0, blue: 81/255.0, alpha: 1)
var blueBG = UIColor(red: 160/255.0, green: 196/255.0, blue: 226/255.0, alpha: 1)

var amountText = UIColor(red: 147/255.0, green: 177/255.0, blue: 219/255.0, alpha: 1)

var payStatusTrue = UIColor(red: 67/255.0, green: 67/255.0, blue: 67/255.0, alpha: 1)
var cgPayStatusTrue = CGColor(red: 67/255.0, green: 67/255.0, blue: 67/255.0, alpha: 1)
var payStatusFalse = UIColor(red: 163/255.0, green: 209/255.0, blue: 209/255.0, alpha: 1)
var cgPayStatusFalse = CGColor(red: 163/255.0, green: 209/255.0, blue: 209/255.0, alpha: 1)
