//
//  DataInfoDetail+CoreDataProperties.swift
//  記帳
//
//  Created by Murphy on 2024/5/1.
//
//

import Foundation
import CoreData


extension DataInfoDetail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DataInfoDetail> {
        return NSFetchRequest<DataInfoDetail>(entityName: "DataInfoDetail")
    }

    @NSManaged public var date: String?
    @NSManaged public var item: String?
    @NSManaged public var itemImg: String?
    @NSManaged public var price: Int32
    @NSManaged public var remark: String?
    @NSManaged public var dataInfo: DataInfo?

}

extension DataInfoDetail : Identifiable {

}
