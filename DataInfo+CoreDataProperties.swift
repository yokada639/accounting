//
//  DataInfo+CoreDataProperties.swift
//  記帳
//
//  Created by Murphy on 2024/5/1.
//
//

import Foundation
import CoreData


extension DataInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DataInfo> {
        return NSFetchRequest<DataInfo>(entityName: "DataInfo")
    }

    @NSManaged public var date: String?
    @NSManaged public var totalPrice: Int32
    @NSManaged public var dataInfoDetail: NSSet?

}

// MARK: Generated accessors for dataInfoDetail
extension DataInfo {

    @objc(addDataInfoDetailObject:)
    @NSManaged public func addToDataInfoDetail(_ value: DataInfoDetail)

    @objc(removeDataInfoDetailObject:)
    @NSManaged public func removeFromDataInfoDetail(_ value: DataInfoDetail)

    @objc(addDataInfoDetail:)
    @NSManaged public func addToDataInfoDetail(_ values: NSSet)

    @objc(removeDataInfoDetail:)
    @NSManaged public func removeFromDataInfoDetail(_ values: NSSet)

}

extension DataInfo : Identifiable {

}
