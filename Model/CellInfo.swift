//
//  CellInfo.swift
//  記帳
//
//  Created by Murphy on 2024/4/6.
//

import Foundation
import CoreData

class CellInfo{
    var totalPrice: String?
    var date: String?
    var detailData: [CellInfoDetail]?
    var objectId: NSManagedObjectID?


}

class CellInfoDetail{
    var item: String?
    var remark: String? = ""
    var price: String?
    var itemImg: String?
    var objectId: NSManagedObjectID?
    var superClassObjectId: NSManagedObjectID?

}
