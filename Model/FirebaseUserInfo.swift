//
//  FirebaseUserInfo.swift
//  記帳
//
//  Created by Murphy on 2024/5/18.
//

import UIKit
import FirebaseFirestore
import FirebaseFirestoreSwift //import FirebaseFirestoreSwift將資料對應到自訂的型別

/**
 Firebase查詢使用者後得到的資料
 */
/// - Parameter uid: Firebase User ID, 等同document ID, 抓到資料時，id 的內容將為資料的 document id
/// - Parameter account: Email帳號
/// - Parameter userName: 使用者暱稱
/// - Parameter ownedLedgers: 自己擁有的帳本
/// - Parameter invitedLedgers: 被邀請的帳本
/// - Parameter unconfirmedLedgers: 待確認的帳本
/// - Parameter enable: 帳號是否廢棄
class FirebaseUserInfo: Identifiable, Codable {
    @DocumentID var uid: String?
    var account: String?
    var userName: String?
    var ownedLedgers: [String]?
    var invitedLedgers: [String]?
    var unconfirmedLedgers: [String]?
    var enable: Bool?    
    
}
