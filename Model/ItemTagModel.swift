//
//  ItemTagModel.swift
//  分類標籤Model
//
//  Created by Murphy on 2024/5/4.
//

import UIKit

class ItemTagModel: NSObject, Codable {
    
    var tagList: [ItemTagModel]?
    var customTagImages: [String] = ["cosmetic", "car", "ingredients-48", "microphone", "home", "airplane", "woman-biking", "doctors-bag", "present", "kid", "pet", "compact-disc-for-music-and-audio-files-48", "toys-48", "stationery-48", "fruits-48", "pc-48", "books-48", "game-48", "investment-48", "movie-48", "appliances-48", "phone-48", "shoes-48", "friend" ]
//                                     "fish-food-48", "flour-48", "grapes-48", "healthy-food-48", "ice-cream-cone-48", "ice-cream-sundae-48", "plum-48", "pumpkin-48", "rice-bowl-48", "soy-48", "strawberry-48", "sugar-cube-48", "sunny-side-up-eggs-48", "thanksgiving-48", "tomato-48", "vegan-food-48", "milk-bottle-48", "mushroom-48", "nut-48", "raspberry-48"]
    var tagText: String?
    var tagImgName: String?
    
    /**
     初始化標籤陣列
     */
    func initArrayInfo(){
        tagList = [ItemTagModel]()
        let tagTexts = ["餐飲","零食","衣服","交通","日用品","購物","保險"]
//                        "化妝品","加油","娛樂","家用","旅行",
//                        "運動","醫療","禮物",
//                        "小孩","寵物"]
        let tagImgNames = ["restaurant", "candy","polo-shirt","bus","box","cart","insurance-48"]
//                           "cosmetic","car","microphone","home","airplane",
//                           "woman-biking","doctors-bag","present",
//                           "kid","pet"]
        
        for i in 0...(tagTexts.count - 1){
            let item = ItemTagModel()
            item.tagText = tagTexts[i]
            item.tagImgName = tagImgNames[i]
            self.tagList?.append(item)
        }
    }    
}
