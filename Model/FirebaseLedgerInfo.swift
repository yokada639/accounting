//
//  FirebaseLedgerInfo.swift
//  記帳
//
//  Created by Murphy on 2024/5/18.
//

import UIKit
import FirebaseFirestore
import FirebaseFirestoreSwift //import FirebaseFirestoreSwift將資料對應到自訂的型別

/**
 Firebase帳本資料
 */
/// - Parameter ledgerUid: Firebase 帳本 ID, 等同document ID, 抓到資料時，id 的內容將為資料的 document id
/// - Parameter ledgerId: Firebase 帳本 ID, 等同document ID
/// - Parameter ownerUid: 擁有者uid
/// - Parameter ownerName: 擁有者姓名
/// - Parameter ownerEmail: 擁有者Email
/// - Parameter ledgerName: 帳本顯示名稱
/// - Parameter sharedUsers: 共享者uid
class FirebaseLedgerInfo: Identifiable, Codable {
    @DocumentID var ledgerUid: String?
    var ledgerId: String?
    var ownerUid: String?
    var ownerName: String?
    var ownerEmail: String?
    var ledgerName: String?
    var sharedUsers: [String]?    
}
