//
//  FirebaseLedgerDetails.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/1.
//

import UIKit
import FirebaseFirestore
import FirebaseFirestoreSwift //import FirebaseFirestoreSwift將資料對應到自訂的型別
/**
 分帳帳務明細
 */
/// - parameter uid 帳務id
/// - parameter ledgerID 所屬帳本id
/// - parameter creatorUid 新增者暱稱
/// - parameter creatorEmail 新增者Email
/// - parameter billTitle 帳務標題
/// - parameter billRemark 帳務備註
/// - parameter payAmount 總金額
/// - parameter payerList 付款名單
class FirebaseLedgerDetails: Identifiable, Codable {
    @DocumentID var uid: String?
    var ledgerID: String?
    var creatorName: String?
    var creatorEmail: String?
    var billTitle: String?
    var billRemark: String?
    var payAmount: String?
    var payerList: [Payer]?
}

/// - parameter payerName 付款者暱稱
/// - parameter payerEmail 付款者email
/// - parameter pay 付款金額
/// - parameter state 狀態（0=尚未添加 1=已被添加)
/// - parameter payStatus 付款狀態（true=已付款 false=未付款）
class Payer: Codable{
    var payerID: String?
    var payerName: String?
    var payerEmail: String?
    var pay: String?
    var state: Int?
    var payStatus: Bool?
}
