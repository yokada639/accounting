//
//  CellDataParam.swift
//  記帳
//
//  Created by Murphy on 2024/4/30.
//

import UIKit

class CellDataParam: NSObject {
    var date: String?
    var item: String?
    var itemImg: String?
    var price: String?
    var remark: String?
    var totalPrice: String? = "0"
}

