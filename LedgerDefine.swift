//
//  LedgerDefine.swift
//  記帳
//
//  Created by Murphy on 2024/5/17.
//

import Foundation
import FirebaseFirestore

//操作Firestore的全域常數，當currentUser有值時才會初始化
var dbParent:Firestore?

//操作UserDefaults的全域常數
let userDefaults = UserDefaults.standard
