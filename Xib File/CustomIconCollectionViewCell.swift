//
//  CustomIconCollectionViewCell.swift
//  記帳
//
//  Created by Murphy on 2024/5/5.
//

import UIKit

class CustomIconCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        // Initialization code
    }

    func setCellImage(imageName: String){
        self.imageView.image = UIImage(named: imageName)
    }
    
}
