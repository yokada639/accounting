//
//  InputView.swift
//  記帳
//
//  Created by Murphy on 2024/4/13.
//

import UIKit
import CoreData



class InputView: UIView {
    
    @IBOutlet weak var itemImg: UIImageView!
    
    @IBOutlet weak var display: UIView!
    
    @IBOutlet weak var displayHeight: NSLayoutConstraint!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var remarkTextfield: UITextField!
    
    @IBOutlet weak var remarkTextFieldHeight: NSLayoutConstraint!
    
    @IBOutlet weak var topBorderView: UIView!
    
    @IBOutlet weak var itemLabel: UILabel!
    
    @IBOutlet weak var btnOutsideViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnView: UIView!
    
    @IBOutlet weak var dateBtn: UIButton!
    var dateBtnText: String?
    
    @IBOutlet weak var itemTableView: UITableView!
    
    @IBOutlet var btnCollection: [UIButton]!
    
    var tmpView: UIView?
    
    var presentDatePickerClosure: (_ date: String?)->() = { _ in}
    
    var closeEditViewClosure: ()->() = {}
    
    var pushEditTagsVCClosure: ()->() = {}
    
    var addDataClosure: (_ newData: CellDataParam)->() = { _ in }
    
    var updateDataClosure: (_ newData: CellDataParam, _ objectID: NSManagedObjectID?)->() = { _ , _ in }
    
    weak var delegate: InputViewDelegate?
    
    var keyboardToolBarTextlabel:UILabel?
    
    @IBOutlet var keyboardToolBar: UIToolbar!
    
    @IBOutlet weak var toolBarCleanBtn: UIBarButtonItem!
    
    @IBOutlet weak var textfieldBtn: UIBarButtonItem!
    
    var swipeGesture:UISwipeGestureRecognizer = UISwipeGestureRecognizer()
    
    var swipeGestureActive: Bool = true
    
    var sendTapGesture:UITapGestureRecognizer = UITapGestureRecognizer()
    
    var cellObjectId: NSManagedObjectID?
    
    @IBOutlet weak var sendInfoBtn: UIImageView!
    
    var itemTagsList:[ItemTagModel]?
    
//  MARK: 用來操作core data的常數
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    /**
     load XIB畫面固定程式碼，UI設定都要在loadXibView之後才能做
     */
    override init(frame: CGRect){
        super.init(frame: frame)
        loadXibView(viewName: "InputView")
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadXibView(viewName: "InputView")
        setDelegateSettings()
        setUIFrames()
        setKeyboardToolBar()
        setCloseEditView()
        //        setSendInfoBtn()
        getItemTags()
        //讓TableView畫面無法彈跳
        itemTableView.bounces = false
        
        
    }
    
//  MARK: - Functions
    /**
    reload畫面
     */
    func reload(){
        loadXibView(viewName: "InputView")
        setDelegateSettings()
        setUIFrames()
        setKeyboardToolBar()
        setCloseEditView()
        //        setSendInfoBtn()
        getItemTags()
        //讓TableView畫面無法彈跳
        itemTableView.bounces = false
    }
    
    /**
    取得標籤陣列+reload table view
     */
    func getItemTags(){
        if let tagList = userDefaults.object(forKey: "itemTagList") as? Data {
            let decoder = JSONDecoder()
            self.itemTagsList = try? decoder.decode([ItemTagModel].self, from: tagList)
        }
        self.itemTableView.reloadData()
    }
    
    /**
     下滑手勢：將editView下縮
     */
    func setCloseEditView(){
        self.swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(setCloseEditViewObjc(_:)))
        swipeGesture.direction = .down
        topBorderView.addGestureRecognizer(swipeGesture)
    }
    @objc func setCloseEditViewObjc(_ gesture: UITapGestureRecognizer) {
        guard swipeGestureActive else { return }
        self.price.text = "0"
        self.remarkTextfield.text = ""
        //MARK: closure寫法
        self.closeEditViewClosure()

        //MARK: delegate寫法
        //self.delegate?.closeInputViewDelegate()
    }
    
    /**
     將xib畫面帶入欲顯示的地方
     */
    func loadXibView(viewName: String){
        if let sub = self.tmpView{
            sub.removeFromSuperview()
        }
        
        //MARK: 本區塊是固定code
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: viewName, bundle: bundle)
        let xibView = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        xibView.frame = bounds
        self.frame = bounds
        self.addSubview(xibView)
        
        //把已添加的xib畫面暫存起來，若之後需修改則先刪處畫面再重新帶入
        self.tmpView = xibView
    }
    
//   MARK: 設定送出按鈕(用程式寫手勢)
//    上方宣告變數：var sendTapGesture:UITapGestureRecognizer = UITapGestureRecognizer()
//    把imageView互動打開不然無法互動（可用選擇imageView勾選User Interaction Enabled或寫程式如下）
    /**
     設定送出按鈕(用程式寫手勢)
     */
    func setAddDataViewAction(){
        sendInfoBtn.isUserInteractionEnabled = true
        self.sendTapGesture.addTarget(self, action: #selector(addDataObjc))
        sendInfoBtn.addGestureRecognizer(sendTapGesture)
    }
    @objc func addDataObjc(_ gesture: UITapGestureRecognizer) {
        print("ok")
    }
    
//   MARK: 設定送出按鈕(用UI拉手勢)
//    先把UITapGestureRecognizer拉近要作用的元件(例如label / imageView)
//    把互動打開(元件勾選User Interaction Enabled)
//    把手勢action拉進來IBAction
    /**
     設定送出按鈕(用UI拉手勢)
     */
    @IBAction func addData(_ sender: Any) {
        let dataParam = getDataParam()
        if let objectId = cellObjectId{
            self.updateDataClosure(dataParam, objectId)
        }
        else{
            self.addDataClosure(dataParam)
        }
    }
    
    /**
    取得使用者輸入欲新增或更新的資料
     */
    func getDataParam() -> CellDataParam {
        let dataParam = CellDataParam()
        if let date = dateBtn.currentTitle{
            dataParam.date = date
        }
        
        if let itemText = itemLabel.text{
            dataParam.item = itemText
            if let tagList = itemTagsList{
                for itemTag in tagList{
                    if(itemTag.tagText == itemText){
                        dataParam.itemImg = itemTag.tagImgName
                        break
                    }
                }
            }
        }

        if let priceText = price.text{
            dataParam.price = priceText
        }
        if let remark = remarkTextfield.text{
            dataParam.remark = remark
        }
        return dataParam
    }
    
    /**
        設定輸入備註時的小鍵盤上方功能列
     */
    func setKeyboardToolBar(){
        let screenWidth = UIScreen.main.bounds.width
        keyboardToolBar.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 50)
        keyboardToolBar.backgroundColor = .gray
        
        
        //編輯的View
        let displayView = UIView(frame: CGRect(x: 0, y: 2, width: screenWidth * 0.75, height: 30))
        displayView.layer.cornerRadius = 15
        displayView.backgroundColor = .white
        
        //View裡面顯示的label
        
        self.keyboardToolBarTextlabel = UILabel(frame: CGRect(x: 10, y: 2, width: screenWidth * 0.75 - 20, height: 28))
        //讓label的字顯示後面打的字
        self.keyboardToolBarTextlabel?.lineBreakMode = .byTruncatingHead
        self.keyboardToolBarTextlabel?.textColor = .black
        self.keyboardToolBarTextlabel?.text = ""
        displayView.addSubview(keyboardToolBarTextlabel ?? UILabel())
        
        //UIBarButtonItem指定畫面
        textfieldBtn.customView = displayView
        
        //監聽remarkTextfield,當文字改變時toolBar工具裡的文字也要改變
        remarkTextfield.addTarget(self, action: #selector(changeTextOnToolbar), for: .editingChanged)

        //textfield的鍵盤小工具指定為keyboardToolBar
        self.remarkTextfield.inputAccessoryView = keyboardToolBar
    }
//       MARK: 同步備註欄位跟螢幕上的label內容，判斷最大值15字
        /**
         同步備註欄位跟螢幕上的label內容
         */
        @objc func changeTextOnToolbar(){
            if let textFieldText = remarkTextfield.text{
                if(textFieldText.count <= 15){
                    self.keyboardToolBarTextlabel?.text = textFieldText
                }
                else{
                    remarkTextfield.text = String(textFieldText.prefix(15))
                    self.keyboardToolBarTextlabel?.text = String(textFieldText.prefix(15))
                }
            }
        }
    
    
    
    /**
        畫面內Frame設定
     */
    func setUIFrames(){
        //display導圓角
        self.display.layer.cornerRadius = display.frame.height / 2
       
        //數字區btn導圓角
        for btn in btnCollection{
            btn.layer.cornerRadius = btn.frame.height / 2
        }
        
        //數字區高度
        self.btnOutsideViewHeight.constant = UIScreen.main.bounds.height * 0.3
        self.btnView.setNeedsLayout()
        self.btnView.layoutIfNeeded()
        
        //dateLabel預設今天日期
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        //btn修改文字內容要用setTitle(預設定之文字, .normal)
        dateBtnText = formatter.string(from: date)
        dateBtn.setTitle(dateBtnText, for: .normal)
        
        //設定備註textField高度
        remarkTextFieldHeight.constant = (display.frame.height-0.5) * 0.45
    }
    
    //   MARK: 判斷price數字格式是否合法:
    //   true:price當下文字不為０ false: price當下文字為０
        /**
            判斷price數字格式是否合法
         */
        func validatePriceFormat() -> Bool{
            let priceLabel = price.text ?? "0"
            if(priceLabel.count == 1 && priceLabel == "0"){
                return false
            }
            else{
                return true
            }
        }
    
//MARK: - IBActions
    /**
        toolbar文字清除
     */
    @IBAction func cleanEditText(_ sender: Any) {
        self.keyboardToolBarTextlabel?.text = ""
        remarkTextfield.text = ""
    }
    
    /**
        變更日期
     */
    @IBAction func changeDate(_ sender: Any) {
        let currentDate = dateBtn.currentTitle
        self.presentDatePickerClosure(currentDate)
    }

    /**
        按數字按鈕修改price
     */
    @IBAction func numberBtnAction(_ sender: UIButton) {
        let priceLabel = self.price.text ?? "0"
        if(priceLabel.count == 17){
            return
        }
        
        if let btnText = sender.titleLabel?.text{            
            if(self.validatePriceFormat()){
                switch(btnText){
                case "1":
                    self.price.text = priceLabel + btnText
                case "2":
                    self.price.text = priceLabel + btnText
                case "3":
                    self.price.text = priceLabel + btnText
                case "4":
                    self.price.text = priceLabel + btnText
                case "5":
                    self.price.text = priceLabel + btnText
                case "6":
                    self.price.text = priceLabel + btnText
                case "7":
                    self.price.text = priceLabel + btnText
                case "8":
                    self.price.text = priceLabel + btnText
                case "9":
                    self.price.text = priceLabel + btnText
                case "0":
                    self.price.text = priceLabel + btnText
                default: return
                }
            }
            else{
                self.price.text = btnText
            }
        }
    }
    
    /**
        btn - ⬅︎
     */
    @IBAction func undoPrice(_ sender: Any) {
        var priceLabel = self.price.text ?? "0"
        if (priceLabel.count > 1 ){
            priceLabel.removeLast()
            self.price.text = priceLabel
        }
        else{
            self.price.text = "0"
        }
        
    }
    
    /**
     btn - AC
     */
    @IBAction func cleanNumber(_ sender: Any) {
        price.text = "0"
    }
    
    /**
     btn - 編輯標籤
     */
    @IBAction func editTags(_ sender: UIButton) {
        self.pushEditTagsVCClosure()
    }
    
}





//MARK: - extension UITableView delegate / dataSource
extension InputView: UITableViewDelegate, UITableViewDataSource{
    /**
      return 一個section中有多少row
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemTagsList?.count ?? 0
    }
    
    /**
        return cell樣式
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InputViewTableViewCell", for: indexPath) as! InputViewTableViewCell
        if let tag = itemTagsList?[indexPath.row]{
            cell.setCell(tag: tag)
        }
        return cell
    }

    /**
        選擇分類時改變標題的類別
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let imgName = itemTagsList?[indexPath.row].tagImgName, let itemName = itemTagsList?[indexPath.row].tagText{
            self.itemImg.image = UIImage(named: imgName)
            self.itemLabel.text = itemName
        }
    }
    
    /**
        設定tableView nib / delegate / data source / textfield delegate
     */
    func setDelegateSettings(){
        let tableViewNib = UINib(nibName: "InputViewTableViewCell", bundle: nil)
        self.itemTableView.register(tableViewNib, forCellReuseIdentifier: "InputViewTableViewCell")
        itemTableView.delegate = self
        itemTableView.dataSource = self
        
        //textfield
        remarkTextfield.delegate = self
    }
    
    /**
     tableView cell將進入畫面內
     */
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        
//    }
    /**
     tableView cell移出畫面外
     */
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//    }
}

//MARK: - extension UITextFieldDelegate
extension InputView: UITextFieldDelegate{
    /**
     在使用者按下 Return 鍵時被呼叫
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // 收起鍵盤
        swipeGestureActive = true
        return true
    }
    
    /**
     在使用者開始編輯文字欄位之前被呼叫
     */
    func textFieldDidBeginEditing(_ textField: UITextField) {
        swipeGestureActive = false
        if(remarkTextfield.text != ""){
            keyboardToolBarTextlabel?.text = remarkTextfield.text
        }
    }
    
}

