//
//  inputViewTableViewCell.swift
//  記帳
//
//  Created by Murphy on 2024/4/13.
//

import UIKit

class InputViewTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImg: UIImageView!
    
    @IBOutlet weak var itemName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(tag: ItemTagModel){
        iconImg.image = UIImage(named: tag.tagImgName ?? "")
        itemName.text = tag.tagText
        itemName.textColor = .black
    }
}
