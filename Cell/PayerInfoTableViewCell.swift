//
//  PayerInfoTableViewCell.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/30.
//

import UIKit

class PayerInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var payStatus: UILabel!
    @IBOutlet weak var changeStatusBtn: UIButton!
    
    var data: PayerInfoTableViewCellData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.payStatus.layer.borderWidth = 0.8
//        if let status = data?.payStatus, status == true{
//            self.payStatus.textColor = payStatusTrue
//            self.payStatus.layer.borderColor = cgPayStatusTrue
//        }
//        else{
//            self.payStatus.textColor = payStatusFalse
//            self.payStatus.layer.borderColor = cgPayStatusFalse
//        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func changeStatusAction(_ sender: UIButton) {
        self.data?.changeStatusClosure()
    }
}

class PayerInfoTableViewCellData: NSObject{
    var userName: String?
    var userEmail: String?
    var amount: String?
    var payStatus: Bool?
    var payStatusTextColor: UIColor?
    var payStatusBorderColor: CGColor?
    var hasChangeStatusBtn: Bool?
    var changeStatusClosure: () -> () = {}
    
    init(userName: String? = nil, userEmail: String? = nil, amount: String? = nil, payStatus: Bool? = nil, payStatusTextColor: UIColor? = nil, payStatusBorderColor: CGColor? = nil, hasChangeStatusBtn: Bool? = nil, changeStatusClosure: @escaping () -> Void = {}) {
        self.userName = userName
        self.userEmail = userEmail
        self.amount = amount
        self.payStatus = payStatus
        self.payStatusTextColor = payStatusTextColor
        self.payStatusBorderColor = payStatusBorderColor
        self.hasChangeStatusBtn = hasChangeStatusBtn
        self.changeStatusClosure = changeStatusClosure
    }
}

extension PayerInfoTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if let data = data as? PayerInfoTableViewCellData{
            self.data = data
            self.nameLabel.text = data.userName
            self.emailLabel.text = data.userEmail
            self.amountLabel.text = "$ \(data.amount ?? "0")"
            self.payStatus.textColor = data.payStatusTextColor
            self.payStatus.layer.borderColor = data.payStatusBorderColor
            if let userPayStatus = data.payStatus{
                if userPayStatus{
                    self.payStatus.text = "已付款"
                }
                else{
                    self.payStatus.text = "未付款"
                }
            }
            self.changeStatusBtn.isHidden = !(data.hasChangeStatusBtn ?? false)
            
        }
    }
    
    
}
