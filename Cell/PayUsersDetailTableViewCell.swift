//
//  PayUsersDetailTableViewCell.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/10.
//

import UIKit

class PayUsersDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var stateButton: UIButton!
    
    var data: PayUsersDetailTableViewCellData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func changeState(_ sender: UIButton) {
        if let user = self.data{
            if self.data?.payUserState == 0{
                self.data?.addPayUserClosure(user)
            }
            else{
                self.data?.removeUserClosure(user)
            }
        }
    }
    
}

/**
 Cell UI Data
 */
/// - Parameter cellIndex 回傳cell的index
/// - Parameter userName user暱稱
/// - Parameter userEmail user Email
/// - Parameter payMoney 金額
/// - Parameter payUserState user 狀態：0=尚未添加 1=已被添加
/// - Parameter addPayUserClosure 將user添加到付款名單
/// - Parameter removeUserClosure 將user從付款名單中移除
class PayUsersDetailTableViewCellData: NSObject{
    var uid: String?
    var userName: String?
    var userEmail: String?
    var payMoney: String?
    var payUserState: Int?
    var addPayUserClosure: (_ userInfo: PayUsersDetailTableViewCellData) -> () = { _ in}
    var removeUserClosure: (_ userInfo: PayUsersDetailTableViewCellData) -> () = { _ in}
    var moneyIsHidden: Bool?
    var imageName: String?
    
    init(uid: String? = nil, userName: String? = nil, userEmail: String? = nil, payMoney: String? = nil, payUserState: Int? = nil, addPayUserClosure: @escaping (_: PayUsersDetailTableViewCellData) -> Void = { _ in}, removeUserClosure: @escaping (_: PayUsersDetailTableViewCellData) -> Void = { _ in}, moneyIsHidden: Bool? = nil, imageName: String? = nil) {
        self.uid = uid
        self.userName = userName
        self.userEmail = userEmail
        self.payMoney = payMoney
        self.payUserState = payUserState
        self.addPayUserClosure = addPayUserClosure
        self.removeUserClosure = removeUserClosure
        self.moneyIsHidden = moneyIsHidden
        self.imageName = imageName
    }

}

extension PayUsersDetailTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if data is PayUsersDetailTableViewCellData{
            self.data = data as? PayUsersDetailTableViewCellData
            self.userNameLabel.text = self.data?.userName
            self.userEmailLabel.text = self.data?.userEmail
            if let moneyIsHidden = self.data?.moneyIsHidden,
                let imageName = self.data?.imageName{
                self.moneyLabel.isHidden = moneyIsHidden
                self.stateButton.setImage(UIImage(named: imageName), for: .normal)
            }
            
            if self.data?.payUserState == 0{
                self.moneyLabel.isHidden = true
                self.stateButton.setImage(UIImage(named: "add-60"), for: .normal)
            }
            else if self.data?.payUserState == 1{
                self.moneyLabel.isHidden = false
                self.moneyLabel.text = (self.data?.payMoney ?? "0") + "元"
                self.stateButton.setImage(UIImage(named: "tools_do-not-disturb-60"), for: .normal)
            }
        }
    }
    
    
}
