//
//  AddedUserTableViewCell.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/23.
//

import UIKit

class AddedUserTableViewCell: UITableViewCell {
    
    var data: AddedUserTableViewCellData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addUserAction(_ sender: UIButton) {
        self.data?.addUserBtnClosure()
    }
}

/**
 Cell UI Data
 */
class AddedUserTableViewCellData: NSObject{
    
    var addUserBtnClosure: () -> () = {}
    
    init(addUserBtnClosure: @escaping () -> Void) {
        self.addUserBtnClosure = addUserBtnClosure
    }
}

extension AddedUserTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if data is AddedUserTableViewCellData{
            self.data = data as? AddedUserTableViewCellData
        }
    }
    
    
}
