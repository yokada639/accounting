//
//  EditTagsTableViewCell.swift
//  記帳
//
//  Created by Murphy on 2024/5/5.
//

import UIKit

class EditTagsTableViewCell: UITableViewCell {

    @IBOutlet weak var tagImage: UIImageView!
    
    @IBOutlet weak var tagName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(tag: ItemTagModel){
        tagImage.image = UIImage(named: tag.tagImgName ?? "")
        tagName.text = tag.tagText
    }
    
}
