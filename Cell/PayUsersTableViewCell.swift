//
//  PayUsersTableViewCell.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/7.
//

import UIKit

/**
 帳本明細頁頁面SharedLedgerDetailVC 的 TableViewCell
 */
class PayUsersTableViewCell: UITableViewCell {
    @IBOutlet weak var payInfoTableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var cellData: [AnyObject]? = []
    var data: PayUsersTableViewCellData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setDelegates()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDelegates(){
        self.payInfoTableView.delegate = self
        self.payInfoTableView.dataSource = self
        let nib = UINib(nibName: "PayUsersDetailTableViewCell", bundle: nil)
        self.payInfoTableView.register(nib, forCellReuseIdentifier: "PayUsersDetailTableViewCell")
    }
    
    
    
//    func reloadData(){
//        self.setCellUIData()
//        self.tableViewHeight.constant = self.payInfoTableView.contentSize.height
//        layoutIfNeeded()
//        setNeedsLayout()
//        self.payInfoTableView.reloadData()
//    }
    
    
    
}

extension PayUsersTableViewCell: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if let tableViewData = cellData?[indexPath.row] {
            switch tableViewData {
            case let cellUIData as PayUsersDetailTableViewCellData:
                if let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PayUsersDetailTableViewCell", for: indexPath) as? PayUsersDetailTableViewCell {
                    tableViewCell.setupCell(data: cellUIData)
                    cell = tableViewCell
                }
            default:
                break
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //設定修改Action（cell左滑）
        
        let updateAction = UIContextualAction(style: .normal, title: "修改") { [weak self] (action, view, completion) in
            guard let strongSelf = self else { return }
            tableView.reloadData()
//            let reloadData: (_ :PayUsersDetailTableViewCellData)->() = { [weak self] user in
//                guard let selfVC = self else { return }
//                selfVC.data?.userList?[indexPath.row] = user
//                strongSelf.payInfoTableView.reloadData()
//            }
            strongSelf.data?.editAmount(indexPath)
        }
        let configuration = UISwipeActionsConfiguration(actions: [updateAction])

        return configuration
    }
}

/**
 Cell UI Data
 */
class PayUsersTableViewCellData: NSObject{
    var userList: [PayUsersDetailTableViewCellData]?
    var updateUser: (_ : PayUsersDetailTableViewCellData?) -> () = { _ in }
    var editAmount: (_: IndexPath) -> () = { _ in }
    
    init(userList: [PayUsersDetailTableViewCellData]? = nil, updateUser: @escaping (_: PayUsersDetailTableViewCellData?) -> Void = { _ in}, editAmount: @escaping (_: IndexPath) -> Void = { _ in }) {
        self.userList = userList
        self.updateUser = updateUser
        self.editAmount = editAmount
    }
    
}

extension PayUsersTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if data is PayUsersTableViewCellData{
            self.data = data as? PayUsersTableViewCellData
            self.cellData = self.data?.userList
            self.setCellUIData()
            self.payInfoTableView.reloadData()
        }
    }
    
    func setCellUIData(){
        self.cellData = []
        if let userList = data?.userList, userList.count > 0{
            let removeClosure: ( _ : PayUsersDetailTableViewCellData?) -> () = { [weak self] user in
                guard let strongSelf = self else { return }
                if let userIndex = userList.firstIndex(where: { $0.userEmail == user?.userEmail }), let oriUser = strongSelf.data?.userList?[userIndex] {
                    oriUser.payUserState = 0
                    oriUser.payMoney = nil
                    strongSelf.cellData?.remove(at: userIndex)
                    strongSelf.data?.updateUser(user)
                }
                strongSelf.payInfoTableView.reloadData()
                
            }
            for user in userList{
                user.removeUserClosure = removeClosure
                self.cellData?.append(user)
            }
        }
    }
}


