//
//  BillDetailTableViewCell.swift
//  Ledgers
//
//  Created by Murphy on 2024/7/7.
//

import UIKit

class BillDetailTableViewCell: UITableViewCell {
    var data: BillDetailTableViewCellData?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var creatorLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}

class BillDetailTableViewCellData: NSObject{
    var title: String?
    var creator: String?
    var remark: String?
    var amount: String?
    
    init(title: String? = nil, creator: String? = nil, remark: String? = nil, amount: String? = nil) {
        self.title = title
        self.creator = creator
        self.remark = remark
        self.amount = amount
    }
}

extension BillDetailTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if let data = data as? BillDetailTableViewCellData{
            self.data = data
            self.titleLabel.text = data.title
            self.creatorLabel.text = "新增者：" + (data.creator ?? "")
            self.remarkLabel.text = "備註：" + (data.remark ?? "無")
            self.amountLabel.text = "$ " + (data.amount ?? "0")
        }
    }
    
    
}
