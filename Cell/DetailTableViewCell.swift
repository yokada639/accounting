//
//  detailTableViewCell.swift
//  記帳
//
//  Created by Murphy on 2024/4/6.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var itemWithRemark: UILabel!
    
    @IBOutlet weak var item: UILabel!
    @IBOutlet weak var remark: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var itemView: UIView!
    
    @IBOutlet weak var itemRemarkView: UIView!
    
    @IBOutlet weak var itemImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setCellDetail(cellInfoDetail: CellInfoDetail){
        if let imgName = cellInfoDetail.itemImg{
            itemImage.image = UIImage(named: imgName)
        }
        if let remarkText = cellInfoDetail.remark{
            if(remarkText.isEmpty){
                self.itemRemarkView.isHidden = true
                self.itemView.isHidden = false
                self.item.text = cellInfoDetail.item
            }
            else{
                self.itemView.isHidden = true
                self.itemRemarkView.isHidden = false
                self.itemWithRemark.text = cellInfoDetail.item
                self.remark.text = remarkText
            }
        }
        
        self.price.text = cellInfoDetail.price
        
        
    }
    
}

