//
//  BillListTableViewCell.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/28.
//

import UIKit

class BillListTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var creatorLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!
    
    var data: BillListTableViewCellData?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func listPageBtnAction(_ sender: UIButton) {
        if let uid = self.data?.uid{
            self.data?.gotoListPageClosure(uid)
        }
    }
}

class BillListTableViewCellData: NSObject{
    var uid: String?
    var title: String?
    var creatorName: String?
    var creatorEmail: String?
    var amount: String?
    var remark: String?
    var gotoListPageClosure:(_: String) -> () = { _ in}
    
    init(uid: String? = nil, title: String? = nil, creatorName: String? = nil, creatorEmail: String? = nil, amount: String? = nil, remark: String? = nil, gotoListPageClosure: @escaping (_: String) -> Void = { _ in}) {
        self.uid = uid
        self.title = title
        self.creatorName = creatorName
        self.creatorEmail = creatorEmail
        self.amount = amount
        self.remark = remark
        self.gotoListPageClosure = gotoListPageClosure
    }
}

extension BillListTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if let data = data as? BillListTableViewCellData{
            self.data = data
            self.titleLabel.text = data.title
            self.creatorLabel.text = "新增者：\(data.creatorName ?? "")"
            self.amountLabel.text = "$ " + (data.amount ?? "0")
            self.remarkLabel.text = "備註：\(data.remark ?? "無")"
        }
    }
    
    
}
