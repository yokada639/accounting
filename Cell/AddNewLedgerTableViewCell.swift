//
//  AddNewLedgerTableViewCell.swift
//  記帳
//
//  Created by Murphy on 2024/5/17.
//

import UIKit
import FirebaseFirestore

class AddNewLedgerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userName: UILabel!
    var data: AddNewLedgerTableViewCellData?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(email: String){
        self.userEmail.text = email
        
    }
}

/**
 Cell UI Data
 */
class AddNewLedgerTableViewCellData: NSObject{
    var email: String?
    var name: String?
    
    init(email: String? = nil, name: String? = nil) {
        self.email = email
        self.name = name
    }
}

extension AddNewLedgerTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if data is AddNewLedgerTableViewCellData {
            self.data = data as? AddNewLedgerTableViewCellData
            self.userEmail.text = self.data?.email
            self.userName.text = self.data?.name
        }
    }
    
    
    
}


