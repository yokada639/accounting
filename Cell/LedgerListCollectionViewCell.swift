//
//  LedgerListCollectionViewCell.swift
//  記帳
//
//  Created by Murphy on 2024/5/11.
//

import UIKit

class LedgerListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ledgerName: UILabel!

    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var outerView: UIView!
    
    var data: LedgerListCollectionViewCellData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        innerView.layer.cornerRadius = 18
        outerView.layer.cornerRadius = 20
        // Initialization code
    }
    
    @IBAction func clickCell(_ sender: UIButton) {
//        if let ledgerId = data?.ledgerId{
//
//        }
        data?.goToledgerDetailClosure()
    }
}

/**
 Cell UI Data
 */
class LedgerListCollectionViewCellData: NSObject{
    var ledgerName: String?
    var goToledgerDetailClosure: () -> () = {}

    init(ledgerName: String? = nil, goToledgerDetailClosure: @escaping () -> Void) {
        self.ledgerName = ledgerName
        self.goToledgerDetailClosure = goToledgerDetailClosure
    }
}

extension LedgerListCollectionViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if data is LedgerListCollectionViewCellData{
            self.data = data as? LedgerListCollectionViewCellData
            self.ledgerName.text = self.data?.ledgerName
        }
    }
}


