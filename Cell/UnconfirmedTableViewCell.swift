//
//  UnconfirmedTableViewCell.swift
//  記帳
//
//  Created by Murphy on 2024/5/20.
//

import UIKit
import FirebaseFirestore

class UnconfirmedTableViewCell: UITableViewCell {
    @IBOutlet weak var ownerName: UILabel!
    
    @IBOutlet weak var ledgerName: UILabel!
    var data: UnconfirmedTableViewCellData?
    
    var reloadTableViewClosure: ()->() = {}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func accept(_ sender: UIButton) {
        self.data?.acceptClosure(self.data?.ledgerId)        
    }
    
    @IBAction func reject(_ sender: UIButton) {
        self.data?.rejectClosure(self.data?.ledgerId)
    }
    

}

/**
 Cell UI Data
 */
class UnconfirmedTableViewCellData: NSObject{
    var ownerName: String?
    var ledgerNameString: String?
    var ledgerId: String?
    var acceptClosure: ( _ : String? )->() = { _ in }
    var rejectClosure: ( _ : String? )->() = { _ in }

    init(ownerName: String? = nil, ledgerNameString: String? = nil, ledgerId: String? = nil, acceptClosure: @escaping (_: String?) -> Void = { _ in }, rejectClosure: @escaping (_: String?) -> Void = { _ in }) {
        self.ownerName = ownerName
        self.ledgerNameString = ledgerNameString
        self.ledgerId = ledgerId
        self.acceptClosure = acceptClosure
        self.rejectClosure = rejectClosure
    }
}

extension UnconfirmedTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if data is UnconfirmedTableViewCellData {
            self.data = data as? UnconfirmedTableViewCellData
            self.ownerName.text = self.data?.ownerName
            self.ledgerName.text = String("邀請你共享：" + (self.data?.ledgerNameString ?? ""))
        }
    }
    
}


