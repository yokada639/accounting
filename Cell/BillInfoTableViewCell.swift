//
//  BillInfoTableViewCell.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/5.
//

import UIKit

class BillInfoTableViewCell: UITableViewCell {
    var data: BillInfoTableViewCellData?
    @IBOutlet weak var titleBG: UIView!
    @IBOutlet weak var remarkBG: UIView!
    @IBOutlet weak var amountBG: UIView!
    
    @IBOutlet weak var remarkTextView: UITextView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleBG.layer.cornerRadius = 20
        self.amountBG.layer.cornerRadius = 20
        self.remarkBG.layer.cornerRadius = 20
        self.titleTextField.delegate = self
        self.amountTextField.delegate = self
        self.remarkTextView.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

/**
 Cell UI Data
 */
class BillInfoTableViewCellData: NSObject{
    var getTitleClosure: (_ title: String?) -> () = { _ in }
    var getAmountClosure: (_ amount: String?) -> () = { _ in }
    var getRemarkClosure: (_ remark: String?) -> () = { _ in }
    
    init(getTitleClosure: @escaping (_: String?) -> Void, getAmountClosure: @escaping (_: String?) -> Void, getRemarkClosure: @escaping (_: String?) -> Void) {
        self.getTitleClosure = getTitleClosure
        self.getAmountClosure = getAmountClosure
        self.getRemarkClosure = getRemarkClosure
    }
}

extension BillInfoTableViewCell: SetupCellProtocol{
    func setupCell(data: Any?) {
        if data is BillInfoTableViewCellData {
            self.data = data as? BillInfoTableViewCellData
        }
    }
}



extension BillInfoTableViewCell: UITextFieldDelegate{
    /**
     在使用者按下 Return 鍵時被呼叫
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // 收起鍵盤
        return true
    }
    
    /**
     當textfield正在輸入時會呼叫（輸入一個字呼叫一次）
     text: 輸入前文字 / textRange: 輸入範圍 / updatedText: 修改後的文字(固定code)
     return true:輸入成功  false: 輸入失敗
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let originText = textField.text,
           let textRange = Range(range, in: originText) {
            let updatedText = originText.replacingCharacters(in: textRange, with: string)
            if textField == self.titleTextField{
                self.data?.getTitleClosure(updatedText)
            }
            else if textField == self.amountTextField{
                if updatedText.hasPrefix("0"){
                    return false
                }
                self.data?.getAmountClosure(updatedText)
            }
        }
        return true
    }
}

extension BillInfoTableViewCell: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let originText = textView.text,
           let textRange = Range(range, in: originText){
            let updateText = originText.replacingCharacters(in: textRange, with: text)
            self.data?.getRemarkClosure(updateText)
        }
        return true
    }
}
