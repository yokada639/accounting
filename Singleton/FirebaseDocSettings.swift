//
//  FirebaseDocSettings.swift
//  記帳
//
//  Created by Murphy on 2024/5/19.
//

import Foundation
import FirebaseFirestore
import FirebaseAuth
/**
 Singleton
 操作全域變數-dbParent
 */
class FirebaseDocSettings: NSObject{
    static let shareInstance = FirebaseDocSettings()
    
    private override init() {}
    
    func getDbParent()-> Bool {
        if Auth.auth().currentUser == nil{
            dbParent = nil
            return false
        }else{
            //全域變數dbParent 賦值
            dbParent = Firestore.firestore()
            //將current user UID存進UserDefault
            userDefaults.set(Auth.auth().currentUser?.uid, forKey: "DBKey")
            return true
        }
    }
    
    /**
     取登入的使用者及使用者owner帳本
     */
    func getcurrentUserAndLedgers(vc:LedgerListViewController){
        if FirebaseDocSettings.shareInstance.getDbParent(){
            Task{
                //MARK: 主執行緒：DispatchQueue.main.async { ... }
                ProgressTool.shareInstance.progressStart()
                await CurrentUserDBInfo.shareInstance.getInfo(vc: vc)
                if await vc.ledgerSegment.selectedSegmentIndex == 0{
                    await vc.getOwnerLedgers()
                    await vc.setUIData(ledgerList: vc.ownerledgerList ?? [])
                }
                else{
                    await vc.getInvitedLedgers()
                    await vc.setUIData(ledgerList: vc.invitedLedgerList ?? [])
                }
                
                ProgressTool.shareInstance.progressEnd()
            }
        }
    }
}
