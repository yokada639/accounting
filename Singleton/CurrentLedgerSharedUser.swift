//
//  CurrentLedgerSharedUser.swift
//  Ledgers
//
//  Created by Murphy on 2024/6/22.
//

import UIKit
/**
 singleton 分享帳本成員名單
 */
class CurrentLedgerSharedUser: NSObject {
    static let shareInstance = CurrentLedgerSharedUser()
    var users: [FirebaseUserInfo]? = [FirebaseUserInfo]()
    var usersID: [String] = []
    
    private override init() {}
    
    func getUsersInfoFromFirebase(ledgerId: String) async{
        var userList:[String]? = [String]()
        if let db = dbParent{
            let docRef = db.collection("Ledger").document(ledgerId)
            do{
                let document = try await docRef.getDocument()
                if document.exists{
                    if let ledgerInfo = try? document.data(as: FirebaseLedgerInfo.self){
                        //兩個array合併可用 array1 + array2
                        userList = ledgerInfo.sharedUsers
                    }
                }
                if let list = userList{
                    self.users = []
                    self.usersID = []
                    for user in list{
                        let docRef = db.collection("User").document(user)
                        do{
                            let document = try await docRef.getDocument()
                            if document.exists{
                                if let userInfo = try? document.data(as: FirebaseUserInfo.self){
                                    self.users?.append(userInfo)
                                    self.usersID.append(userInfo.uid ?? "")
                                }
                            }
                        }
                        catch{
                            print("get user List from Firebase")
                            print(error.localizedDescription)
                        }
                    }
                    
                }
            }
            catch{
                print("get Ledger info from Firebase")
                print(error.localizedDescription)
            }
        }
    }
    
    func getUsersUID() -> [String]{
        return self.usersID
    }
    
}
