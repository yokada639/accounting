//
//  CurrentUserDBInfo.swift
//  記帳
//
//  Created by Murphy on 2024/5/22.
//

import Foundation
import UIKit
import FirebaseAuth

/**
 singleton 登入者資訊
 */
class CurrentUserDBInfo: NSObject{
    static let shareInstance = CurrentUserDBInfo()
    var myInfo: FirebaseUserInfo?
    
    private override init() {}
    
    func getInfo(vc viewController: UIViewController? = nil) async{
        let selfUid = userDefaults.string(forKey: "DBKey") ?? ""
        //MARK: docRef：設定路徑
         if let db = dbParent{
            let docRef = db.collection("User").document(selfUid)
            do {
                let document = try await docRef.getDocument()
                if document.exists {
                    let userInfo = try? document.data(as: FirebaseUserInfo.self)
                    print("CurrentUserDBInfo.shareInstance.myInfo")
                    self.myInfo = userInfo
                }
                else{
//                    errorUser(vc: viewController)
                }
            }
            catch {
              print("Error getting document: \(error)")
            }
        }
    }
    
    /**
     取不到當前使用者資訊即呼叫alert
     */
    func errorUser(vc: UIViewController?){
        let alertController = UIAlertController(title: nil, message: "資料處理失敗，請重新登入", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default){ [weak self] _ in
            guard let selfVC = self else { return }
            vc?.dismiss(animated: true)
        }
        alertController.addAction(okAction)
        DispatchQueue.main.async {
            vc?.present(alertController, animated: true)
            try? Auth.auth().signOut()
            userDefaults.removeObject(forKey: "DBKey")
            vc?.tabBarController?.selectedIndex = 0
        }
        
        
    }
}
