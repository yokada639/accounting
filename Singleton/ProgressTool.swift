//
//  ProgressTool.swift
//  Ledgers
//
//  Created by Murphy on 2024/9/4.
//

import UIKit
import ProgressHUD
class ProgressTool: NSObject {
    static let shareInstance = ProgressTool()
    
    private override init() {}
    
    func progressStart(){
        ProgressHUD.animationType = .circleArcDotSpin
        ProgressHUD.colorAnimation = yellowBG
        ProgressHUD.animate("Loading...",interaction: false)
    }
    
    func progressEnd(){
        ProgressHUD.dismiss()
    }
}
