//
//  LoadingManager.swift
//  記帳
//
//  Created by Murphy on 2024/5/8.
//

import UIKit

/**
 製作Loding View(純code)：滿版mask+ 活動指示器UIActivityIndicatorView (中間灰色轉圈圖示)
 */
class LoadingManager: NSObject {
    
    //使用static的manager來操作loading頁面出現與否
    static let manager = LoadingManager()
    
    private var maskView: UIView?
    private var activityIndicator: UIActivityIndicatorView?
    
    /**
     初始化活動指示器和mask
     */
    func initActivityIndicator(view: UIView) {
        maskView = UIView(frame: view.bounds)  //view.bounds = 跟view尺寸一樣大
        maskView?.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        maskView?.isHidden = true
        
        activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true //動畫停止時，活動指示器就隱藏(正常而言default值為true)
        
        maskView?.addSubview(activityIndicator!)
        view.addSubview(maskView!)

    }
    
    /**
     啟動活動指示器
     */
    func startLoading() {
        maskView?.isHidden = false
        activityIndicator?.startAnimating()
    }
    
    /**
     停止活動指示器
     */
    func stopLoading() {
        activityIndicator?.stopAnimating()
        maskView?.isHidden = true
    }
}
