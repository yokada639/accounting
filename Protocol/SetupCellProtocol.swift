//
//  File.swift
//  記帳
//
//  Created by Murphy on 2024/5/22.
//

import Foundation

/**
 設定Cell Data的protocol
 */
protocol SetupCellProtocol {
    func setupCell( data: Any? )
}
