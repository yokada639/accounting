//
//  DetailViewControllerDelegate.swift
//  記帳
//
//  Created by Murphy on 2024/5/1.
//

import Foundation
import UIKit

protocol DetailViewControllerDelegate: AnyObject {

    func setCurrentDateDelegate(date: String?)
}
